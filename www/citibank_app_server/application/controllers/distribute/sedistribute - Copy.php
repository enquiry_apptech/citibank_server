<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Sedistribute extends CI_Controller {
    
    var $application_id;
    
    const model_name = 'sedistribute_model';
    const user_model_name='user_model';
    const model_log='log_model';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model($this::model_name);
        $this->load->model($this::user_model_name);
        $this->load->model($this::model_log);
        
        $this->load->model("log_expiry_date_model");
        
        $this->model_object = $this->sedistribute_model;
        $this->load->library('userrole/UserModule');
    }
   
    //used in the list all, show the application id
    //should set application id first, then use
    public function Send_App_ID(){
        echo $this->session->userdata('application_id');
    }
    
    //called when scan application id
    //f:if_appid_used
    //f:set_value(application_id, $app_id)
    public function Store_AppID(){
        $success = false;
        if($this->user_model->checked_if_login()){
        //add $_post to get the mobile data
        $app_id=$_POST['app_id']; 
        //$app_id=123123;		
		if($this->model_object->if_appid_used($app_id)){
            //log the application id has been used before, the app terminate immediately
        }
        else{
		    $this->model_object->store_application_id($app_id);
            $success=true;
        }
        }
        echo $success;   
    }   
    
    //search Coupon Type by Application Type
    //f:search_coupon_info(($app_type_id)
    public function Search_Coupon_Type(){
        $app_type_id=$_POST['app_type_id'];
        if($this->user_model->checked_if_login()){
        //add $_post to get the mobile data
        $stack_array=$this->model_object->search_coupon_info($app_type_id);
        //return $stack_array;
        echo json_encode($stack_array);
        }
        else{
            echo null;
            //redirect to login page
        }
    }
    
    //triggered after scanning the coupon
    //return true or false if coupon used successfully or not
    //f:if_coupid_used($coupon_id)
    //f:store_used_coupon($application_id, $coupon_id)
    public function Store_Used_Coupon(){
        $coupon_id=$_POST['coupon_id'];
        $coupon_type_id=$_POST['coupon_type_id'];
		$app_type_id=$_POST['app_type_id'];
        $success=false;
        if($this->user_model->checked_if_login()){
        
        if($this->model_object->if_coupid_used($coupon_id)){
            //log the coupon has been used then stop
            //return false;
        } else {
            //Check whether the coupon number is in range
            $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);
            $start = $coupon_result[0]->sequence_start_digit;
            $end = $coupon_result[0]->sequence_end_digit;
            
            $in_range = false;
            $expiry_report_query = $this->log_expiry_date_model->get_by_coupon_name($coupon_type_uid, $year);
        
            if ($expiry_report_query->num_rows() > 0){
                $result_set = $expiry_report_query->result();

                foreach ($result_set as $row){
                    //check if it is in range
                    $expiry_date_sequence_range = $row->expiry_date_sequence_range;

                    $pos = strrpos($expiry_date_sequence_range, "/");
                    if ($pos === false) { // note: three equal signs
                        // not found...
                        $sequence_start = $expiry_date_sequence_range;
                        $sequence_end = $expiry_date_sequence_range;
                    } else {
                        $pieces = explode("/", $expiry_date_sequence_range);
                        $sequence_start = $pieces[0];
                        $sequence_end = $pieces[1];
                    }

                    $length = (int) $end - (int) $start + 1;
                    $sequence_start_substring = substr($sequence_start, $start-1, $length);
                    $sequence_end_substring = substr($sequence_end, $start-1, $length);

                    $coupon_id_substring = substr($coupon_id, $start-1, $length);

                    if ((int)$coupon_id_substring <= (int) $sequence_end_substring
                            && (int)$coupon_id_substring >= (int) $sequence_start_substring){
                        $in_range = true;
                    }
                }
            }
            
            if (!$in_range){
                //Log
                $content=" [Fault] Coupon No.".$coupon_id." is not in range.";
                $category="coupon_error";
                $this->log_model->update_log($content,$category);
            }
            
            
		//if($this->model_object->sequence_checking($coupon_type_id, $coupon_id)){
            $application_id=$this->session->userdata('application_id');
            if($this->model_object->store_used_coupon($application_id, $app_type_id, $coupon_id,$coupon_type_id)){			    
                $success=true;
            }
            else{
                
            }
			//}
			//else{
			//$category="coupon_error";
            //$content="[Fault] Coupon No"+$coupon_id+" Does not exist.";
            //$this->log_model->update_log($content,$category);
			//}
        }
        }
        echo $success;
    }
    

	//triggered after upload Offline Records
    //return true or false if coupon used successfully or not
    //f:if_coupid_used($coupon_id)
    //f:store_used_coupon($application_id, $coupon_id)
	public function Store_Offline_Coupon(){
		$json=$_POST['jsonString'];
		$user_login_id=$_POST['user_login_id'];
		$success=false;
		$failed=false;
		$result="";
		$content = json_decode($json);
		foreach ($content as $key){
			if($this->model_object->offline_if_appid_used($key->app_id,$user_login_id)){
				//log the application id has been used before, the app terminate immediately
				$success=false;
				$failed=true;
				$result=$result."ApplicationID:".$key->app_id." duplicated!\n";
			}
			
			if($this->model_object->store_application_id($key->app_id)){
				foreach($key->coupons as $coupon){

					if($this->model_object->offline_if_coupid_used($coupon->Coup_id,$user_login_id)){
					  //log the coupon has been used then stop
					  //return false;
					  $failed=true;
					  $success=false;
					  $result=$result."CouponID:".$coupon->Coup_id." is used.\n";
					}
					
					if($this->model_object->store_used_coupon($key->app_id, $coupon->App_type_id, $coupon->Coup_id,$coupon->Coup_tid)){          
						$success=true;
					}				
				  }
				}
		}
		
		if($failed)
			echo $result;
		else
			echo $success;
	}
	
	
    //search all the coupon simply by application id, used in show
    //triggered after finish
    //f:list_all_coupon($app_id) 
    public function Show_All_Coupon(){
	
	
	$app_id=$this->session->userdata('application_id');
	$application_uid = 0;
	$application_uid_hello=0;
	$application_uid_dsds=0;
	$coupon_number=0;
    $query = $this->db->get_where('app_application', array('app_id' => $app_id));
    foreach ($query->result() as $row){
        $application_uid = $row->application_uid;
    }
	$query_app = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid));
	foreach ($query_app->result() as $row){
        $coupon_number = $row->coupon_number;
    }
	if ($coupon_number == NULL){
	$this->db->delete("app_application",array('app_id'=>$app_id));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	}
	$this->db->delete("app_app_coupon",array('coupon_number'=>"undefined"));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	$this->db->delete("app_application",array('app_id'=>NULL));
	if ( (strlen($coupon_number) == 0) || ($coupon_number == '0') || ($coupon_number == 'null')){
	$this->db->delete("app_application",array('app_id'=>$app_id));
	$this->db->delete("app_app_coupon",array('coupon_number'=>$coupon_number));
	
	}
	
	$query_e = $this->db->get_where('app_app_coupon', array('coupon_number' => NULL));
    foreach ($query_e->result() as $row){
        $application_uid_hello = $row->application_uid;
		$this->db->delete('app_application',array('application_uid'=>$application_uid_hello));
    }
	$this->db->select('application_uid');
    $this->db->from('app_application');
	$query_a = $this->db->get();
	 if ($query_a->num_rows() > 0){
        foreach($query_a->result() as $row){
			$application_uid_dsds = $row->application_uid;	
			$query_sasa = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid_dsds));
			
			if ($query_sasa->num_rows() == 0){
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			}
			if ($query_sasa->num_rows() > 0){
        foreach($query_sasa->result() as $row){
		    $cp = $row->coupon_number;
			if($cp == NULL){
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			$this->db->delete("app_app_coupon",array('coupon_number'=>$cp));
			
			}
		}
			}
        }
	 }
	 
	 
	 //authorized
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
        $stack = array();
        if($this->user_model->checked_if_login()){
        $application_id=$this->session->userdata('application_id');
        $stack=$this->model_object->list_all_coupon($application_id);
        $this->user_model->logout();
		$this->session->set_userdata('application_id',-1);
		
        //return $stack;
        echo json_encode($stack);
        }
        else{
            //redirect to login page
            echo null;
        }
    }
	
	
	 public function get_coupon_add(){
        $stack = array();
        if($this->user_model->checked_if_login()){
            $role_id = $this->usermodule->get_user_role();
            $stack=$this->model_object->find_coupon_add($role_id);
            echo json_encode($stack);
        }
        else{
            echo null;
        }
    } 
}

