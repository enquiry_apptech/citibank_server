<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class User extends CI_Controller{
    const model_name = 'user_model';
    const model_log='log_model';
    const model_dis='sedistribute_model';
    
    public function __construct(){
        parent::__construct();
        $this->load->model($this::model_name);
        $this->load->model($this::model_log);
        $this->load->model($this::model_dis);
        $this->model_object = $this->user_model;
    }
    
   
    //handle the login page
    public function login(){
        //username and password is posted from mobile app
        //return false, ask the user to change password, otherwise the user cannot log in
        $username=$_POST['username'];
        $password=$_POST['password'];
        $this->session->set_userdata('user_role_uid', $username);
        $success=false;
        if($this->sedistribute_model->if_password_not_changed($username, $password)){
        if($this->model_object->login($username,$password)){
            
             $success=true;
        }
        }
		else{
		echo "hello";
		}
        echo ($success)?"true":"false";
    }
    
    public function reset_password(){
	    $username=$_POST['username'];
        $old_password=$_POST['old_password'];
        $new_password1=$_POST['new_password1'];
        $new_password2=$_POST['new_password2'];
		$this->session->set_userdata('user_role_uid', $username);
        $success=false;
        if($this->model_object->checked_old_password($old_password)){
            $this->model_object->reset_new_password($new_password1,$new_password2);
            $success=true;
        }
        echo ($success)?"true":"false";
    }
    
	//new add function used in log out
	public function logout(){
	$success=false;
	$app_id=$this->session->userdata('application_id');
	$application_uid = 0;
	$application_uid_hello=0;
	$application_uid_dsds=0;
	$coupon_number=0;
    $query = $this->db->get_where('app_application', array('app_id' => $app_id));
    foreach ($query->result() as $row){
        $application_uid = $row->application_uid;
    }
	$query_app = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid));
	foreach ($query_app->result() as $row){
        $coupon_number = $row->coupon_number;
    }
	if ($coupon_number == NULL){
	$this->db->delete("app_application",array('app_id'=>$app_id));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	}
	$this->db->delete("app_app_coupon",array('coupon_number'=>"undefined"));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	$this->db->delete("app_application",array('app_id'=>NULL));
	if ( (strlen($coupon_number) == 0) || ($coupon_number == '0') || ($coupon_number == 'null')){
	$this->db->delete("app_app_coupon",array('coupon_number'=>$coupon_number));
	$this->db->delete("app_application",array('app_id'=>$app_id));
	}
	
	$query_e = $this->db->get_where('app_app_coupon', array('coupon_number' => NULL));
    foreach ($query_e->result() as $row){
        $application_uid_hello = $row->application_uid;
		$this->db->delete('app_application',array('application_uid'=>$application_uid_hello));
    }
	$this->db->select('application_uid');
    $this->db->from('app_application');
	$query_a = $this->db->get();
	 if ($query_a->num_rows() > 0){
        foreach($query_a->result() as $row){
			$application_uid_dsds = $row->application_uid;	
			$query_sasa = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid_dsds));
			
			if ($query_sasa->num_rows() == 0){
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			}
			if ($query_sasa->num_rows() > 0){
        foreach($query_sasa->result() as $row){
		    $cp = $row->coupon_number;
			if($cp == NULL){
			$this->db->delete("app_app_coupon",array('coupon_number'=>$cp));
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			}
		}
			}
        }
	 }
	
	
	
	if($this->model_object->logout()){
	    $success=true;
	}
	echo ($success)?"true":"false";
	}
}

