<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coupon extends CI_Controller {

    //Define a constant that hold the path of the all the view files
    const view_dir_path = 'admin_platform/';
    
    const model_name = 'coupon_model';
    
    const user_model_name = 'user_model';
    const image_model_name = 'image_model';
    const sysparam_model_name = 'sysparam_model';
    const coupon_onhand_model_name = 'coupon_onhand_model';
    const coupon_receive_model_name = 'coupon_receive_model';
    
    const object_name = 'Coupon';
    const module_name = 'coupon';
    const uid_field_name = "coupon_type_uid";
    
    var $model_object = null;
    var $user_model_object = null;
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model($this::model_name);
        $this->load->model($this::user_model_name);
        $this->load->model($this::image_model_name);
        $this->load->model($this::sysparam_model_name);
        $this->load->model($this::coupon_onhand_model_name);
        $this->load->model($this::coupon_receive_model_name);
        $this->load->model("log_expiry_date_model");
        $this->load->model("log_model");
        
        $this->model_object = $this->coupon_model;
        $this->user_model_object = $this->user_model;
        $this->load->library('lib_admin/CRUDLibrary');
        $this->load->library('lib_admin/ImageLibrary');
        $this->load->library('userrole/UserModule');
    }

    /**
     * @desc list all records
     * @param 
     * @return
     */
    
    public function listall() {
        $this->usermodule->restrict_role_login('super');
        $this->crudlibrary->listall($this, 'coupon');
    }

    public function create() {
        $this->usermodule->restrict_role_login('super');
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/create');
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function edit($object_uid) {
        $this->usermodule->restrict_role_login('super');
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        $query_result = $this->coupon_model->get_by_primary_key($object_uid);
        $content_data['object'] = $query_result[0];
        
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/edit', $content_data);
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function calculate_quantity() {
        $coupon_type_uid = $_POST['coupon_type_uid'];
        $sequence_start = $_POST['sequence_start'];
        $sequence_end = $_POST['sequence_end'];
        $coupon_number = $_POST['coupon_number'];
        $expiry_date = $_POST['expiry_date'];
        
        $result = $this->model_object->get_by_primary_key($coupon_type_uid);
        $coupon_name = $result[0]->name;
        
        if ($coupon_number=='') {
            //create multiple quantity
            $query_result = $this->coupon_model->get_sequence_digits_by_coupon_type_uid($coupon_type_uid);
            $start = $query_result[0]->sequence_start_digit;
            $end = $query_result[0]->sequence_end_digit;

            $length = (int) $end - (int) $start + 1;
            $sequence_start_substring = substr($sequence_start, $start-1, $length);
            $sequence_end_substring = substr($sequence_end, $start-1, $length);
            
            if (is_numeric($sequence_start_substring) && is_numeric($sequence_end_substring)){
                $quantity = (int) $sequence_end_substring - (int) $sequence_start_substring + 1;
                if ($quantity<0){
                    echo "format_error";
                    return;
                }
            } else {
                echo "format_error";
                return;
            }
        } else {
            //create single coupon quantity
            $sequence_start = $coupon_number;
            $sequence_end = $coupon_number;
            
            $quantity = 1;
        }
        
        $result_array = array("coupon_name"=>$coupon_name, "quantity"=>$quantity);
        
        echo json_encode($result_array);
    }
    
    public function stock_in() {
        $this->usermodule->restrict_role_login('super');
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        //Get the coupon list
        $this->db->select('coupon_type.name option_name, coupon_type.coupon_type_uid option_value');
        $this->db->from('cou_coupon_type coupon_type');
//        $this->db->join('usr_user user_table', 'manager.user_uid = user_table.user_uid' , 'left');
        
        $query = $this->db->get();
        $coupon_type_array = $query->result();
        
        $content_data['coupon_type_array'] = $coupon_type_array;
        $content_data['module_name'] = $this::module_name;
        
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/stock_in', $content_data);
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function stock_in_receive() {
        $this->usermodule->restrict_role_login('super');
        
        $coupon_type_uid = $_POST['coupon_type_uid'];
        $sequence_start = $_POST['sequence_start'];
        $sequence_end = $_POST['sequence_end'];
        $coupon_number = $_POST['coupon_number'];
        
        $expiry_date = $_POST['expiry_date'];
        
        //detect number of coupons
        //get the sequence_start_digit and sequence_end_digit
        
        $query_result = $this->coupon_model->get_sequence_digits_by_coupon_type_uid($coupon_type_uid);
        $coupon_name = $query_result[0]->name;
        if ($coupon_number==''){
            //create multiple quantity
            
            $start = $query_result[0]->sequence_start_digit;
            $end = $query_result[0]->sequence_end_digit;

            $length = (int) $end - (int) $start + 1;
            $sequence_start_substring = substr($sequence_start, $start-1, $length);
            $sequence_end_substring = substr($sequence_end, $start-1, $length);

            $quantity = (int) $sequence_end_substring - (int) $sequence_start_substring + 1;
        } else {
            //create single coupon quantity
            $sequence_start = $coupon_number;
            $sequence_end = $coupon_number;
            
            $quantity = 1;
        }
        
        $user_uid = 1;//SAM
        
        //check if the coupon ohhand is already exist
        $query = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $user_uid);
        
        if ($query->num_rows() > 0){
            $onhand_result = $query->result();
            $update_ohand_uid = $onhand_result[0]->coupon_onhand_uid;
            
            $updating_onhand_model = $this->coupon_onhand_model->get_by_primary_key($update_ohand_uid);
            
            //create coupon on hand to SAM team
            $coupon_onhand_model = new Coupon_onhand_model();
            
            $coupon_onhand_model->set_value('coupon_onhand_uid', $update_ohand_uid);
            $coupon_onhand_model->set_value('user_uid', $updating_onhand_model[0]->user_uid);
            $coupon_onhand_model->set_value('coupon_type_uid', $updating_onhand_model[0]->coupon_type_uid);
            $coupon_onhand_model->set_value('quantity', (int)$updating_onhand_model[0]->quantity + (int)$quantity);
            $coupon_onhand_model->set_value('sequence_start', $updating_onhand_model[0]->sequence_start);
            $coupon_onhand_model->set_value('sequence_end', $updating_onhand_model[0]->sequence_end);
            $coupon_onhand_model->set_value('expiry_date', '');//no need expiry date

            $this->coupon_onhand_model->update($coupon_onhand_model);
        } else {
            //create coupon on hand to SAM team
            $coupon_onhand_model = new Coupon_onhand_model();
            $coupon_onhand_model->set_value('user_uid', $user_uid);
            $coupon_onhand_model->set_value('coupon_type_uid', $coupon_type_uid);
            $coupon_onhand_model->set_value('quantity', $quantity);
            $coupon_onhand_model->set_value('sequence_start', $sequence_start);
            $coupon_onhand_model->set_value('sequence_end', $sequence_end);
            $coupon_onhand_model->set_value('expiry_date', '');//no need expiry date

            $this->coupon_onhand_model->insert($coupon_onhand_model);
        }
        
        //Create expiry log data
        $log_expiry_date_model = new Log_expiry_date_model();
        $log_expiry_date_model->set_value('expiry_date_coupon_type_uid', $coupon_type_uid);
        $log_expiry_date_model->set_value('expiry_date_coupon_name', $coupon_name);
        $log_expiry_date_model->set_value('quantity', $quantity);
        $log_expiry_date_model->set_value('expiry_date_sequence_range', $sequence_start."/".$sequence_end);
        $log_expiry_date_model->set_value('expiry_date', $expiry_date);
        
        $this->log_expiry_date_model->insert($log_expiry_date_model);
        
        $this->coupon_model->log_movement("[Stock in] $coupon_name with quantity $quantity", $coupon_name, $coupon_type_uid, "in", $quantity, "SAM", "SAM", 1, 1);
        
        echo "success";
//        redirect(base_url().index_page()."/admin_platform/coupon/stock_in", 'refresh');
    }
    
    public function coupon_receive_reject() {
        $coupon_receive_uid = $_POST['coupon_receive_uid'];
        
        //Remove the current coupon accept
        $this->coupon_receive_model->reject_receive($coupon_receive_uid);
        
        echo "success";
    }
    
    public function coupon_receive_accept() {
        $coupon_receive_uid = $_POST['coupon_receive_uid'];
        $target_user_uid = $this->session->userdata('user_uid_using');
        
        //cheking the value of the accepting coupon
        $coupon_receive_result = $this->coupon_receive_model->get_by_primary_key($coupon_receive_uid)->result();
        $assign_quantity = $coupon_receive_result[0]->quantity;
        $coupon_onhand_uid = $coupon_receive_result[0]->coupon_onhand_uid;
        $distributer_uid = $coupon_receive_result[0]->distributer_uid;
        
        $coupon_onhand_result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        
        $coupon_type_uid = $coupon_onhand_result[0]->coupon_type_uid;
        
        $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);
        $coupon_value = $coupon_result[0]->value;
        $coupon_name = $coupon_result[0]->name;
        
        $current_assigning_value = (int)$coupon_value * (int)$assign_quantity;
        
        $excess = false;
        if ($this->session->userdata('brightsystem_login_role') == "super"){
            
        } else {
            //Get the current user max coupon value
            if ($this->session->userdata('brightsystem_login_role') == "manager"){
                $object = $this->manager_model->get_by_user_uid($target_user_uid)->result();
                $max_coupon_value = $object[0]->max_coupon_value;
            } else if ($this->session->userdata('brightsystem_login_role') == "am"){
                $object = $this->am_model->get_by_user_uid($target_user_uid)->result();
                $max_coupon_value = $object[0]->max_coupon_value;
            } else if ($this->session->userdata('brightsystem_login_role') == "se"){
                $object = $this->se_model->get_by_user_uid($target_user_uid)->result();
                $max_coupon_value = $object[0]->max_coupon_value;
            }

            $onhand_coupon_value = $this->_user_onhand_coupon_value($target_user_uid);

            if ($max_coupon_value - $onhand_coupon_value - $current_assigning_value < 0){
                $excess = true;
            }
        }
        if (!$excess){
            $updating_coupon_onhand_model = new Coupon_onhand_model();
            $updating_coupon_onhand_model->set_value("coupon_onhand_uid", $coupon_onhand_result[0]->coupon_onhand_uid);
            $updating_coupon_onhand_model->set_value("user_uid", $coupon_onhand_result[0]->user_uid);
            $updating_coupon_onhand_model->set_value("coupon_type_uid", $coupon_onhand_result[0]->coupon_type_uid);
            $updating_coupon_onhand_model->set_value("quantity", (int)$coupon_onhand_result[0]->quantity - (int)$assign_quantity);
            $updating_coupon_onhand_model->set_value("sequence_start", $coupon_onhand_result[0]->sequence_start);
            $updating_coupon_onhand_model->set_value("sequence_end", $coupon_onhand_result[0]->sequence_end);
            $updating_coupon_onhand_model->set_value("expiry_date", $coupon_onhand_result[0]->expiry_date);

            $this->coupon_onhand_model->update($updating_coupon_onhand_model);

            //check if the coupon ohhand is already exist
            $query = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $target_user_uid);

            if ($query->num_rows() > 0){
                $onhand_result = $query->result();
                $update_ohand_uid = $onhand_result[0]->coupon_onhand_uid;

                $updating_onhand_model = $this->coupon_onhand_model->get_by_primary_key($update_ohand_uid);

                //create coupon on hand to SAM team
                $coupon_onhand_model = new Coupon_onhand_model();

                $coupon_onhand_model->set_value('coupon_onhand_uid', $update_ohand_uid);
                $coupon_onhand_model->set_value('user_uid', $updating_onhand_model[0]->user_uid);
                $coupon_onhand_model->set_value('coupon_type_uid', $updating_onhand_model[0]->coupon_type_uid);
                $coupon_onhand_model->set_value('quantity', (int)$updating_onhand_model[0]->quantity + (int)$assign_quantity);
                $coupon_onhand_model->set_value('sequence_start', $updating_onhand_model[0]->sequence_start);
                $coupon_onhand_model->set_value('sequence_end', $updating_onhand_model[0]->sequence_end);
                $coupon_onhand_model->set_value('expiry_date', '');//no need expiry date

                $this->coupon_onhand_model->update($coupon_onhand_model);
            } else {
                //Create new coupon on hand
                $new_coupon_onhand_model = new Coupon_onhand_model();
                $new_coupon_onhand_model->set_value("user_uid", $target_user_uid);
                $new_coupon_onhand_model->set_value("coupon_type_uid", $coupon_onhand_result[0]->coupon_type_uid);
                $new_coupon_onhand_model->set_value("quantity", (int)$assign_quantity);
                $new_coupon_onhand_model->set_value("sequence_start", $coupon_onhand_result[0]->sequence_start);
                $new_coupon_onhand_model->set_value("sequence_end", $coupon_onhand_result[0]->sequence_end);
                $new_coupon_onhand_model->set_value("expiry_date", $coupon_onhand_result[0]->expiry_date);

                $this->coupon_onhand_model->insert($new_coupon_onhand_model);
            }
            
//            Remove the current coupon accept
//            $this->coupon_receive_model->delete($coupon_receive_uid);
            
            //Update the receive status, add "_close" to the status
            $this->coupon_receive_model->close_receive($coupon_receive_uid);
            
            $user = $this->user_model_object->get_by_primary_key($target_user_uid);
            $name = $user[0]->user_nick_name;
            $distributer_user = $this->user_model_object->get_by_primary_key($distributer_uid);
            $distributer_name = $distributer_user[0]->user_nick_name;
            
            $this->coupon_model->log_movement("[Receive] $name confirm receive $assign_quantity $coupon_name", $coupon_name, $coupon_type_uid, "in", $assign_quantity, $distributer_name, $name, $distributer_uid, $target_user_uid);
            $this->coupon_model->log_log("[Receive] $name confirm receive $assign_quantity $coupon_name", "receive_distribute", $distributer_uid);
        
            
            echo "success";
        } else {
            echo "excess";
        }
    }
    
    private function _user_onhand_coupon_value($target_user_uid) {
        $value_result = $this->coupon_onhand_model->get_all_for_coupon_distribution($target_user_uid);
        $onhand_coupon_value = 0;
        foreach ($value_result as $row){
            $onhand_coupon_value += (int)$row->quantity * (int)$row->value;
        }
        return $onhand_coupon_value;
    }
    
    public function coupon_onhand_stat() {
        $target_user_uid = $_POST['user_uid'];
        
        $onhand_coupon_value = $this->_user_onhand_coupon_value($target_user_uid);
        
        $max_coupon_value = 0;
        
        $role_name = $this->session->userdata('brightsystem_login_role');
        
        //Find the supervised coupon value
        //Get the target user max coupon value
        if ($this->session->userdata('brightsystem_login_role') == "super"){
            $object = $this->manager_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        } else if ($this->session->userdata('brightsystem_login_role') == "manager"){
            $object = $this->am_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        } else if ($this->session->userdata('brightsystem_login_role') == "am"){
            $object = $this->se_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        }
        
        $array = array("max_coupon_value"=>$max_coupon_value,
                        "onhand_coupon_value"=>$onhand_coupon_value
                );
        echo json_encode($array);
    }
    
    public function coupon_onhand_stat_for_return() {
        $target_user_uid = $_POST['user_uid'];
        
        $onhand_coupon_value = $this->_user_onhand_coupon_value($target_user_uid);
        
        $max_coupon_value = 0;
        
        $role_name = $this->session->userdata('brightsystem_login_role');
        
        //Find the supervised coupon value
        //Get the target user max coupon value
        if ($this->session->userdata('brightsystem_login_role') == "manager"){
//            $object = $this->manager_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = "--";
        } else if ($this->session->userdata('brightsystem_login_role') == "am"){
            $object = $this->manager_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        } else if ($this->session->userdata('brightsystem_login_role') == "se"){
            $object = $this->am_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        }
        
        $array = array("max_coupon_value"=>$max_coupon_value,
                        "onhand_coupon_value"=>$onhand_coupon_value
                );
        echo json_encode($array);
    }
    
    public function distribution_confirm() {
        $coupon_name = $_POST['coupon_name'];
        $current_quantity = $_POST['current_quantity'];
        $coupon_onhand_uid = $_POST['coupon_onhand_uid'];
        $assign_quantity = $_POST['assign_quantity'];
        $coupon_value = $_POST['coupon_value'];
        $target_user_uid = $_POST['user_uid'];
        
        $current_user_uid = $this->session->userdata('user_uid_using');
        
        //Get the user object
        $user = $this->user_model_object->get_by_primary_key($target_user_uid);
        $user_nick_name = $user[0]->user_nick_name;
        $current_user = $this->user_model_object->get_by_primary_key($current_user_uid);
        $current_user_nick_name = $current_user[0]->user_nick_name;
        
        $coupon_onhand_result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        $coupon_type_uid = $coupon_onhand_result[0]->coupon_type_uid;
        
        $t=time();
        $today = date("Y-m-d H:i:s",$t);
        
        $role_name = $this->session->userdata('brightsystem_login_role');
        
        if ($role_name=="super" || $role_name=="manager"){
            //Add record to receive table to let other people confirm the receive
            $new_model = new Coupon_receive_model();
            $new_model->set_value("distributer_uid", $current_user_uid);
            $new_model->set_value("receiver_uid", $target_user_uid);
            $new_model->set_value("coupon_type_uid", $coupon_type_uid);
            $new_model->set_value("quantity", $assign_quantity);
            $new_model->set_value("distribute_date", $today);
            $new_model->set_value("status", "distribution");
            $new_model->set_value("coupon_onhand_uid", $coupon_onhand_uid);

            $this->coupon_receive_model->insert($new_model);
            
        } else if ($role_name=="am"){
            //AM distribute to sales, not need confirmation, so direct add onhand record
            
            //Find the corresponding onhand record
            //check if the coupon ohhand is already exist
            $query = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $target_user_uid);

            if ($query->num_rows() > 0){
                //If found, update the quantity
                $onhand_result = $query->result();
                $update_ohand_uid = $onhand_result[0]->coupon_onhand_uid;

                $updating_onhand_model = $this->coupon_onhand_model->get_by_primary_key($update_ohand_uid);

                //create coupon on hand to SAM team
                $coupon_onhand_model = new Coupon_onhand_model();

                $coupon_onhand_model->set_value('coupon_onhand_uid', $update_ohand_uid);
                $coupon_onhand_model->set_value('user_uid', $updating_onhand_model[0]->user_uid);
                $coupon_onhand_model->set_value('coupon_type_uid', $updating_onhand_model[0]->coupon_type_uid);
                $coupon_onhand_model->set_value('quantity', (int)$updating_onhand_model[0]->quantity + (int)$assign_quantity);
                $coupon_onhand_model->set_value('sequence_start', $updating_onhand_model[0]->sequence_start);
                $coupon_onhand_model->set_value('sequence_end', $updating_onhand_model[0]->sequence_end);
                $coupon_onhand_model->set_value('expiry_date', '');//no need expiry date

                $this->coupon_onhand_model->update($coupon_onhand_model);
            } else {
                //If not found, create a new onhand record
                //Create new coupon on hand
                $new_coupon_onhand_model = new Coupon_onhand_model();
                $new_coupon_onhand_model->set_value("user_uid", $target_user_uid);
                $new_coupon_onhand_model->set_value("coupon_type_uid", $coupon_type_uid);
                $new_coupon_onhand_model->set_value("quantity", (int)$assign_quantity);
//                $new_coupon_onhand_model->set_value("sequence_start", $coupon_onhand_result[0]->sequence_start);
//                $new_coupon_onhand_model->set_value("sequence_end", $coupon_onhand_result[0]->sequence_end);
//                $new_coupon_onhand_model->set_value("expiry_date", $coupon_onhand_result[0]->expiry_date);

                $this->coupon_onhand_model->insert($new_coupon_onhand_model);
            }
            
            //Minus the coupon quantity
            $updating_coupon_onhand_model = new Coupon_onhand_model();
            $updating_coupon_onhand_model->set_value("coupon_onhand_uid", $coupon_onhand_result[0]->coupon_onhand_uid);
            $updating_coupon_onhand_model->set_value("user_uid", $coupon_onhand_result[0]->user_uid);
            $updating_coupon_onhand_model->set_value("coupon_type_uid", $coupon_onhand_result[0]->coupon_type_uid);
            $updating_coupon_onhand_model->set_value("quantity", (int)$coupon_onhand_result[0]->quantity - (int)$assign_quantity);
            $updating_coupon_onhand_model->set_value("sequence_start", $coupon_onhand_result[0]->sequence_start);
            $updating_coupon_onhand_model->set_value("sequence_end", $coupon_onhand_result[0]->sequence_end);
            $updating_coupon_onhand_model->set_value("expiry_date", $coupon_onhand_result[0]->expiry_date);

            $this->coupon_onhand_model->update($updating_coupon_onhand_model);
        }
        
        $log_wording = "distribute";
        
        if ($role_name=="super" || $role_name=="manager"){
            //keep the original wording
            //log message
            $this->coupon_model->log_log("[Distribute] $current_user_nick_name $log_wording $coupon_name with quantity $assign_quantity", "receive_distribute", $target_user_uid);
        
        } else if ($role_name=="am"){
            $log_wording = "distributed";
            //log movement
            $this->coupon_model->log_movement("[Distribute]  $log_wording $coupon_name with quantity $assign_quantity to $user_nick_name", $coupon_name, $coupon_type_uid, "out", $assign_quantity, $current_user_nick_name, $user_nick_name, $current_user_uid, $target_user_uid);
        }
        
        echo "success";
    }
    
    public function se_return_confirm() {
        $success=true;
//        $coupon_name = $_POST['coupon_name'];
        $coupon_type_uid = $_POST['coupon_type_uid'];
//        $coupon_onhand_uid = $_POST['coupon_onhand_uid'];
        $assign_quantity = 1;
        $user_login_id = $_POST['user_login_id'];
        
        $se_user = $this->user_model->get_by_user_login_id($user_login_id)->result();
        $se_user_uid = $se_user[0]->user_uid;
        
        $coupon_id = $_POST['coupon_number'];
        
        $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);
//        $coupon_value = $coupon_result[0]->value;
        $coupon_name = $coupon_result[0]->name;
        
        $coupon_onhand_result = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $se_user_uid);
        $coupon_onhand_result_row = $coupon_onhand_result->result();
        $coupon_onhand_record = $coupon_onhand_result_row[0];
        $coupon_onhand_uid = $coupon_onhand_record->coupon_onhand_uid;
        
        //find related am
        $am_user_uid=0;
        $this->db->select('am_uid');
        $this->db->from('rol_se');
        $this->db->where('user_uid', $se_user_uid); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $am_uid = $row->am_uid;
            }
        } else {   
        
        }
        
        $am_result = $this->am_model->get_by_primary_key($am_uid);
        $target_user_uid = $am_result[0]->user_uid;
        
        //minus the quantity from coupon onhand
        $result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        
//        $current_user_uid = $this->session->userdata('user_uid_using');
        
        $t=time();
        $today = date("Y-m-d H:i:s",$t);

        $new_model = new Coupon_receive_model();
        $new_model->set_value("distributer_uid", $se_user_uid);
        $new_model->set_value("receiver_uid", $target_user_uid);
        $new_model->set_value("coupon_type_uid", $result[0]->coupon_type_uid);
        $new_model->set_value("quantity", $assign_quantity);
        $new_model->set_value("distribute_date", $today);
        $new_model->set_value("status", "return");
        $new_model->set_value("coupon_onhand_uid", $coupon_onhand_uid);
        
        $this->coupon_receive_model->insert($new_model);
        
        $this->db->flush_cache();
        
        //check whether coupon_id has been used or not
        $this->db->select('coupon_number');
        $this->db->from('app_app_coupon');
        $this->db->where('coupon_number', $coupon_id); 
        $query2 = $this->db->get();
        if ($query2->num_rows() > 0){
            //log the coupon id has been used before, do not recommend to accept
            $content=" [Duplicate] Coupon No.".$coupon_id." has already been used.";
			$category="coupon_error";
            $this->log_model->update_log($content,$category);
            $success=false;
        }
        
        $this->db->flush_cache();
        
        //Rico: check whether the coupon is returned before
        $this->db->select('coupon_number');
        $this->db->from('cou_return');
        $this->db->where('coupon_number', $coupon_id);
        
        $query3 = $this->db->get();
        if ($query3->num_rows() > 0){
            //log the coupon id has been used before, do not recommend to accept
            $content=" [Duplicate] Coupon No.".$coupon_id." has already been returned.";
            $category="coupon_error";
            $this->log_model->update_log($content,$category);
            $success=false;
        } else {
            $this->db->flush_cache();
            //insert it to the cou_return
            
            //Request a new uid
            $new_uid_result = $this->uidgenerator->request_uid("cou_return");
            $new_uid = $new_uid_result[0]->uid_gen_current_uid;

            $value_array = array("return_uid"=>$new_uid, 
                                "se_user_uid"=>$se_user_uid,
                                "am_user_uid"=>$am_user_uid,
                                "quantity"=>$assign_quantity,
                                "coupon_type_uid"=>$result[0]->coupon_type_uid,
                                "coupon_number"=>$coupon_id,
                                );

            $this->db->insert('cou_return', $value_array);
        }
        
        //Check whether the coupon number is in range
        $start = $coupon_result[0]->sequence_start_digit;
        $end = $coupon_result[0]->sequence_end_digit;
        
        $year = 'all';
        $expiry_report_query = $this->log_expiry_date_model->get_by_coupon_name($coupon_type_uid, $year);
        
        $in_range = false;
        if ($expiry_report_query->num_rows() > 0){
            $result_set = $expiry_report_query->result();
            
            foreach ($result_set as $row){
                //check if it is in range
                $expiry_date_sequence_range = $row->expiry_date_sequence_range;
                
                $pos = strrpos($expiry_date_sequence_range, "/");
                if ($pos === false) { // note: three equal signs
                    // not found...
                    $sequence_start = $expiry_date_sequence_range;
                    $sequence_end = $expiry_date_sequence_range;
                } else {
                    $pieces = explode("/", $expiry_date_sequence_range);
                    $sequence_start = $pieces[0];
                    $sequence_end = $pieces[1];
                }
                
                $length = (int) $end - (int) $start + 1;
                $sequence_start_substring = substr($sequence_start, $start-1, $length);
                $sequence_end_substring = substr($sequence_end, $start-1, $length);
                
                $coupon_id_substring = substr($coupon_id, $start-1, $length);

                if ((int)$coupon_id_substring <= (int) $sequence_end_substring
                        && (int)$coupon_id_substring >= (int) $sequence_start_substring){
                    $in_range = true;
                }
            }
        }
        
        if (!$in_range){
            //Log
            $content=" [Validation] Coupon No.".$coupon_id." has not in range.";
            $category="coupon_error";
            $this->log_model->update_log($content,$category);
        }
                
//        $user = $this->user_model_object->get_by_primary_key($target_user_uid);
//        $user_nick_name = $user[0]->user_nick_name;
        
        $current_user = $this->user_model_object->get_by_primary_key($se_user_uid);
        $current_user_nick_name = $current_user[0]->user_nick_name;
//        $this->coupon_model->log_movement("[Return] $current_user_nick_name return $coupon_name with quantity $assign_quantity to $user_nick_name", $coupon_name, "out", $assign_quantity, $current_user_nick_name, $user_nick_name, $current_user_uid, $target_user_uid);
        $this->coupon_model->log_log("[Return] $current_user_nick_name return $coupon_name with quantity $assign_quantity", "receive_distribute", $target_user_uid);
        
        if ($success){
            echo "success";
        } else {
            echo "fail";
        }
        
    }
    
    public function return_confirm() {
        $coupon_name = $_POST['coupon_name'];
        $current_quantity = $_POST['current_quantity'];
        $coupon_onhand_uid = $_POST['coupon_onhand_uid'];
        $assign_quantity = $_POST['assign_quantity'];
        $coupon_value = $_POST['coupon_value'];
        $target_user_uid = $_POST['user_uid'];
        
        //minus the quantity from coupon onhand
        $result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        
        $current_user_uid = $this->session->userdata('user_uid_using');
        
        $t=time();
        $today = date("Y-m-d H:i:s",$t);

        $new_model = new Coupon_receive_model();
        $new_model->set_value("distributer_uid", $current_user_uid);
        $new_model->set_value("receiver_uid", $target_user_uid);
        $new_model->set_value("coupon_type_uid", $result[0]->coupon_type_uid);
        $new_model->set_value("quantity", $assign_quantity);
        $new_model->set_value("distribute_date", $today);
        $new_model->set_value("status", "return");
        $new_model->set_value("coupon_onhand_uid", $coupon_onhand_uid);
        
        $this->coupon_receive_model->insert($new_model);
        
        $user = $this->user_model_object->get_by_primary_key($target_user_uid);
        $user_nick_name = $user[0]->user_nick_name;
        $current_user = $this->user_model_object->get_by_primary_key($current_user_uid);
        $current_user_nick_name = $current_user[0]->user_nick_name;
//        $this->coupon_model->log_movement("[Return] $current_user_nick_name return $coupon_name with quantity $assign_quantity to $user_nick_name", $coupon_name, "out", $assign_quantity, $current_user_nick_name, $user_nick_name, $current_user_uid, $target_user_uid);
        $this->coupon_model->log_log("[Return] $current_user_nick_name return $coupon_name with quantity $assign_quantity", "receive_distribute", $target_user_uid);
        
        echo "success";
    }
    
    public function return_add() {
        $coupon_onhand_uid = $_POST['coupon_onhand_uid'];
        $assign_quantity = $_POST['quantity'];
        $target_user_uid = $_POST['user_uid'];
        
        //Check if the current add excess the max coupon value
        //Get the target user max coupon value
        $max_coupon_value = 0;
        $role_name = $this->session->userdata('brightsystem_login_role');
        $current_user_uid = $this->session->userdata('user_uid_using');
        
        $coupon_onhand_result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        $coupon_type_uid = $coupon_onhand_result[0]->coupon_type_uid;
            
        $excess = false;
        if ($role_name == "manager"){
//            $object = $this->am_model->get_by_user_uid($target_user_uid)->result();
//            $max_coupon_value = $object[0]->max_coupon_value;
        } else if ($role_name == "am"){
            $object = $this->manager_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
            
            $onhand_coupon_value = $this->_user_onhand_coupon_value($target_user_uid);

            $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);
            $coupon_value = $coupon_result[0]->value;
            $coupon_name = $coupon_result[0]->name;

            $current_assigning_value = (int)$coupon_value * (int)$assign_quantity;

            if ($max_coupon_value - $onhand_coupon_value - $current_assigning_value < 0){
                $excess = true;
            }
        }
        
        $error_message = "";
        //Only for SE case, can not distribute if excess
        if ($role_name == "am" && $excess){
            $error_message = "Excess max coupon value, can not add ".$assign_quantity." $coupon_name to SE";
        }
        
        //Find the distributing quantity
        $distributing_qty = 0;
        $receive_query = $this->coupon_receive_model->get_by_distributer_uid_coupon_type_uid($current_user_uid, $coupon_type_uid);
        
        if ($receive_query->num_rows() > 0){
            $receive_results = $receive_query->result();
            foreach ($receive_results as $receive){
                $distributing_qty += (int)$receive->quantity;
            }
        }
        
        $result = $this->coupon_onhand_model->get_all_for_coupon_distribution_add($coupon_onhand_uid);

        $array = array("coupon_name"=>$result[0]->name,
                        "coupon_type_uid"=>$result[0]->coupon_type_uid,
                        "distributing_qty"=>$distributing_qty,
                        "current_quantity"=>$result[0]->quantity,
                        "coupon_onhand_uid"=>$coupon_onhand_uid,
                        "assign_quantity"=>$assign_quantity,
                        "coupon_value"=>$result[0]->value,
                        "user_uid"=>$target_user_uid,
                        "error_message"=>$error_message
                );

        echo json_encode($array);
    }
    
    public function distribution_add() {
        $coupon_onhand_uid = $_POST['coupon_onhand_uid'];
        $assign_quantity = $_POST['quantity'];
        $target_user_uid = $_POST['user_uid'];
        
        //Check if the current add excess the max coupon value
        //Get the target user max coupon value
        $max_coupon_value = 0;
        $role_name = $this->session->userdata('brightsystem_login_role');
        $current_user_uid = $this->session->userdata('user_uid_using');
        
        if ($role_name == "manager"){
            $object = $this->am_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        } else if ($role_name == "am"){
            $object = $this->se_model->get_by_user_uid($target_user_uid)->result();
            $max_coupon_value = $object[0]->max_coupon_value;
        }
        $onhand_coupon_value = $this->_user_onhand_coupon_value($target_user_uid);

        $coupon_onhand_result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);
        $coupon_type_uid = $coupon_onhand_result[0]->coupon_type_uid;
        
        $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);
        $coupon_value = $coupon_result[0]->value;
        $coupon_name = $coupon_result[0]->name;
        
        $current_assigning_value = (int)$coupon_value * (int)$assign_quantity;
        
        $excess = false;
        if ($max_coupon_value - $onhand_coupon_value - $current_assigning_value < 0){
            $excess = true;
        }
        
        $error_message = "";
        //Only for SE case, can not distribute if excess
        if ($role_name == "am" && $excess){
            $error_message = "Excess max coupon value, can not add ".$assign_quantity." $coupon_name to SE";
        }
        
        //Find the distributing quantity
        $distributing_qty = 0;
        $receive_query = $this->coupon_receive_model->get_by_distributer_uid_coupon_type_uid($current_user_uid, $coupon_type_uid);
        
        if ($receive_query->num_rows() > 0){
            $receive_results = $receive_query->result();
            foreach ($receive_results as $receive){
                $distributing_qty += (int)$receive->quantity;
            }
        }
        
        $result = $this->coupon_onhand_model->get_all_for_coupon_distribution_add($coupon_onhand_uid);

        $array = array("coupon_name"=>$result[0]->name,
                        "coupon_type_uid"=>$result[0]->coupon_type_uid,
                        "distributing_qty"=>$distributing_qty,
                        "current_quantity"=>$result[0]->quantity,
                        "coupon_onhand_uid"=>$coupon_onhand_uid,
                        "assign_quantity"=>$assign_quantity,
                        "coupon_value"=>$result[0]->value,
                        "user_uid"=>$target_user_uid,
                        "error_message"=>$error_message
                );

        echo json_encode($array);
    }
    
    public function distribute() {
        $rolename_array = array('super', 'manager', 'am');
        $this->usermodule->restrict_multiple_role_login($rolename_array);
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        $content_data['module_name'] = $this::module_name;
        
        //Find the people under supervision
        $role_name = $this->session->userdata('brightsystem_login_role');
        $current_user_uid = $this->session->userdata('user_uid_using');
        
        if ($role_name=="super"){
            //SAM, find the managers
            $result = $this->manager_model->get_all_for_coupon_distribution();
            $content_data['user_data'] = $result;
        } else if ($role_name=="manager"){
            //Need to find the supervising manager
            $manager_result = $this->manager_model->get_by_user_uid($current_user_uid)->result();
            $manager_uid = $manager_result[0]->manager_uid;
            
            $result = $this->am_model->get_all_for_coupon_distribution_by_manager_uid($manager_uid);
            $content_data['user_data'] = $result;
        } else if ($role_name=="am"){
            //Need to find the supervising am
            $am_result = $this->am_model->get_by_user_uid($current_user_uid)->result();
            $am_uid = $am_result[0]->am_uid;
            
            $result = $this->se_model->get_all_for_coupon_distribution_by_am_uid($am_uid);
            $content_data['user_data'] = $result;
        }
        
        //Coupon onhand data
        $result = $this->coupon_onhand_model->get_all_for_coupon_distribution($current_user_uid);
        $content_data['coupon_data'] = $result;
        
        $query = $this->db->get_where("sys_param", array("param_uid" => 1));
        $distribution_remarks = $query->result();
        $content_data['distribution_remarks'] = $distribution_remarks[0]->operation_text;
         
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/distribute', $content_data);
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function return_coupon() {
        $rolename_array = array('super', 'manager', 'am');
        $this->usermodule->restrict_multiple_role_login($rolename_array);
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        $content_data['module_name'] = $this::module_name;
        
        //Find the people under supervision
        $role_name = $this->session->userdata('brightsystem_login_role');
        $current_user_uid = $this->session->userdata('user_uid_using');
        
//        $current_user = $this->user_model_object->get_by_primary_key($current_user_uid);
//        $current_user_nick_name = $current_user[0]->user_nick_name;
        
        if ($role_name=="manager"){
            //SAM, find the managers
//            $result = $this->manager_model->get_all_for_coupon_distribution();
            $result = array();
            $obj = new stdClass();
            $obj->user_uid = 1;
            $obj->user_nick_name = "SAM";
            array_push($result, $obj);
            $content_data['user_data'] = $result;
        } else if ($role_name=="am"){
            //Find the supervising manager
            $am_result = $this->am_model->get_by_user_uid($current_user_uid)->result();
            $manager_uid = $am_result[0]->manager_uid;
            
            $result = $this->manager_model->get_by_primary_key_with_user($manager_uid);
            $content_data['user_data'] = $result;
        } else if ($role_name=="se"){
            //No  this case for the CMS
//            //Find the supervising am
//            $se_result = $this->se_model->get_by_user_uid($current_user_uid)->result();
//            $am_uid = $se_result[0]->am_uid;
//            
//            $result = $this->am_model->get_by_primary_key_with_user($am_uid);
//            $content_data['user_data'] = $result;
        }
        
        //Coupon onhand data
        $result = $this->coupon_onhand_model->get_all_for_coupon_distribution($current_user_uid);
        $content_data['coupon_data'] = $result;
        
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/return_coupon', $content_data);
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function receive($month="all") {
        $rolename_array = array('super', 'manager', 'am');
        $this->usermodule->restrict_multiple_role_login($rolename_array);
        
        //Get the current object data
        $header_data['module_name'] = $this::module_name;
        $header_data['user_role'] = $this->session->userdata('brightsystem_login_role');
        
        //Get the login user uid
        $current_user_uid = $this->session->userdata('user_uid_using');
        $role_name = $this->session->userdata('brightsystem_login_role');
        
        //Find out the receive data according to this user
        $query_result = $this->coupon_receive_model->get_by_user_uid_for_receive_record($current_user_uid, $role_name);
        
        
        $query_result_history = $this->coupon_receive_model->get_by_user_uid_for_receive_record_history($current_user_uid, $role_name, $month);
        
        $content_data['query_result'] = $query_result;
        $content_data['query_result_history'] = $query_result_history;
        
        $content_data['month'] = $month;
        
        $this->load->view($this::view_dir_path.'admin_header', $header_data);
        $this->load->view('admin_platform/coupon/receive', $content_data);
        $this->load->view($this::view_dir_path.'admin_footer');
    }
    
    public function insert() {
        $this->usermodule->restrict_role_login('super');
        
        //check duplicate coupon name
        $coupon_name = $_POST['name'];
        $all_record = $this->model_object->get_all();
        
        foreach ($all_record as $each_record){
            $current_coupon_name = $each_record->name;
            
            $escaped_current_name = str_replace(' ', '', $current_coupon_name);
            $escaped_name = str_replace(' ', '', $coupon_name);
            
            if ($escaped_name == $escaped_current_name){
                
                header("Content-Type:text/html; charset=utf-8");
                echo "The name '$escaped_current_name' is already in the database.";
                exit;
            }
        }
        
        $fields = $this->db->list_fields($this->model_object->table_name);
        
        $new_coupon_model = new Coupon_model();
        foreach ($fields as $field_name) {
            if (isset($_POST[$field_name])) {
                $new_coupon_model->set_value($field_name, $_POST[$field_name]);
            }
        }

        if ($this->model_object->insert($new_coupon_model)) {
            redirect(base_url() . index_page() . "/admin_platform/coupon/listall", 'refresh');
        } else {
            
        }
    }
    
    public function update($object_uid) {
        $this->usermodule->restrict_role_login('super');
        
        $fields = $this->db->list_fields($this->model_object->table_name);
        
        $update_model = new Coupon_model();
        $update_model->set_value("coupon_type_uid", $object_uid);
        foreach ($fields as $field_name) {
            if (isset($_POST[$field_name])) {
                $update_model->set_value($field_name, $_POST[$field_name]);
            }
        }
        
        $this->model_object->update($update_model);
        redirect(base_url() . index_page() . "/admin_platform/coupon/listall", 'refresh');
    }
    
    public function delete($object_uid) {
        $this->usermodule->restrict_role_login('super');

        //checking
        //if any user has onhand coupon
        $query = $this->coupon_onhand_model->get_by_coupon_type_uid($object_uid);
        
        if ($query->num_rows()>0){
            //can not delete
            echo "There are users holding this coupon in the system. <br/>";
            echo "Coupons can not be deleted.";
        } else {
            $this->model_object->delete($object_uid);

            redirect(base_url() . index_page() . "/admin_platform/coupon/listall", 'refresh');
        }
        
    }
    
    private function _sequence_checking($coupon_type_uid, $scan_or_input_coupon_number) {
        //Find the stocked in coupons sequences of the coupon type
        $user_uid = 1; //Sam account's user uid
        
        //Selec all the coupon ohhand of SAM of particular coupon type
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        $this->db->where('coupon_onhand.coupon_type_uid', $coupon_type_uid);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            //Check whether it matches any sequence
            $sam_onhand_coupon_result = $query->result();
            
            //For each result, check if the sequence is inside
            foreach($sam_onhand_coupon_result as $onhand_result){
                $sequence_start = $onhand_result->sequence_start;
                $sequence_end = $onhand_result->sequence_end;
                $start = $onhand_result->sequence_start_digit;
                $end = $onhand_result->sequence_end_digit;

                $length = (int) $end - (int) $start + 1;
                $sequence_substring = substr($scan_or_input_coupon_number, $start-1, $length);
                $sequence_start_substring = substr($sequence_start, $start-1, $length);
                $sequence_end_substring = substr($sequence_end, $start-1, $length);
            
                $success = false;
                //Check if it is a number
                if (!is_numeric($sequence_substring)){
//                    echo "format_error";
                    return false;
                } else if ((int)$sequence_substring >= (int)$sequence_start_substring && (int)$sequence_substring <= (int)$sequence_end_substring){
//                    echo "success";
                    $success = true;
                    return true;
                }
            }
            return $success;
        } else {
            return false;
        }
    }
    
}
?>