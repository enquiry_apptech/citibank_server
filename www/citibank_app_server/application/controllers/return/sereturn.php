<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Sereturn extends CI_Controller {
    
    const model_name = 'sereturn_model';
    const user_model_name='user_model';
    const model_log='log_model';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model($this::model_name);
        $this->load->model($this::user_model_name);
        $this->load->model($this::model_log);
        $this->load->model("coupon_model");
        $this->load->model("coupon_onhand_model");
        $this->load->model("coupon_receive_model");
        $this->load->model("am_model");
        $this->load->model("log_expiry_date_model");
        
        $this->model_object = $this->sereturn_model;
        $this->load->library('userrole/UserModule');
    }
    
    //return handler, but only change cou_coupon_receive but not couu_coupon_onhand
    public function return_coupon(){
        $success = true;
		
		$coupon_id=$_POST['coupon_id'];
        $coupon_type_id=$_POST['coupon_type_id'];
		//$coupon_id=123123;
		//$coupon_type_id=1009;
       if($this->user_model->checked_if_login()){
        $user_role_id=$this->usermodule->get_user_role();
        $am_id = $this->model_object->find_related_am($user_role_id);
		//if($this->model_object->sequence_checking($coupon_type_id, $coupon_id)){
        $success=$this->model_object->return_coupon($user_role_id, $am_id, $coupon_type_id,$coupon_id);
		//}
       }
		
        echo ($success) ? 'true' : 'false';
    }
    
//    public function return_coupon_in_batch() {
//        $success = true;
//
//        $coupon_id = $_POST['coupon_id'];
//        $coupon_type_id = $_POST['coupon_type_id'];
//
//        $coupon_id_array = explode(",", $coupon_id);
//        $coupon_type_id_array = explode(",", $coupon_type_id);
//        
//        for ($i = 0; $i < $coupon_id_array; $i++){
//            if ($this->user_model->checked_if_login()) {
//                $user_role_id = $this->usermodule->get_user_role();
//                $am_id = $this->model_object->find_related_am($user_role_id);
//                //if($this->model_object->sequence_checking($coupon_type_id, $coupon_id)){
//                $current_success = $this->model_object->return_coupon($user_role_id, 
//                        $am_id, 
//                        $coupon_type_id_array[$i], 
//                        $coupon_id_array[$i]);
//                //}
//                if ($current_success==false){
//                    $success = $current_success;
//                }
//            }
//        }
//        
//        echo ($success) ? 'true' : 'false';
//    }
    
    public function se_return_confirm_in_batch() {
        $success_array = array();
        $coupon_type_uid = $_POST['coupon_type_id'];
        $coupon_id = $_POST['coupon_id'];
        
        $coupon_id_array = explode(",", $coupon_id);
//        $coupon_type_id_array = explode(",", $coupon_type_uid);
        
        $overall_array_to_store = array();
        $success_log_string_array = array();
        $overall_success = true;
        
        $se_user_uid = $this->usermodule->get_user_role();

        //find related am
        $am_user_uid=0;
        $this->db->select('am_uid');
        $this->db->from('rol_se');
        $this->db->where('user_uid', $se_user_uid); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $am_uid = $row->am_uid;
            }
        } else {   

        }

        $am_result = $this->am_model->get_by_primary_key($am_uid);
        $target_user_uid = $am_result[0]->user_uid;

        $success_quantity = 0;
        for ($i = 0; $i < sizeof($coupon_id_array); $i++){
            $success=true;
            
            $coupon_result = $this->coupon_model->get_by_primary_key($coupon_type_uid);

            $coupon_name = $coupon_result[0]->name;

            $coupon_onhand_result = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $se_user_uid);
            $coupon_onhand_result_row = $coupon_onhand_result->result();
            
            $error_code = 0;
            if (sizeof($coupon_onhand_result_row)>0){

                $this->db->flush_cache();

                //check whether coupon_id has been used or not
                $this->db->select('coupon_number');
                $this->db->from('app_app_coupon');
                $this->db->where('coupon_number', $coupon_id_array[$i]); 
                $query2 = $this->db->get();
                if ($query2->num_rows() > 0){
                    //log the coupon id has been used before, do not recommend to accept
                    $content = " [Duplicate] Coupon No.".$coupon_id_array[$i]." has already been used.";
                    $category = "coupon_error";
                    $this->log_model->update_log($content,$category);
                    $success=false;
                    
                    //Log the error in the response
                    $error_code = 1;
                }

                $this->db->flush_cache();

                //Rico: check whether the coupon is returned before
                $this->db->select('coupon_number');
                $this->db->from('cou_return');
                $this->db->where('coupon_number', $coupon_id_array[$i]);

                $query3 = $this->db->get();
                if ($query3->num_rows() > 0){
                    //log the coupon id has been used before, do not recommend to accept
                    $content=" [Duplicate] Coupon No.".$coupon_id_array[$i]." has already been returned.";
                    $category="coupon_error";
                    $this->log_model->update_log($content,$category);
                    $success=false;

                    //Log the error in the response
                    $error_code = 2;
                } else {
                    $this->db->flush_cache();
                    //insert it to the cou_return

                    //Request a new uid
                    $new_uid_result = $this->uidgenerator->request_uid("cou_return");
                    $new_uid = $new_uid_result[0]->uid_gen_current_uid;

                    $value_array = array("return_uid"=>$new_uid, 
                                        "se_user_uid"=>$se_user_uid,
                                        "am_user_uid"=>$am_user_uid,
                                        "quantity"=>$assign_quantity,
                                        "coupon_type_uid"=>$result[0]->coupon_type_uid,
                                        "coupon_number"=>$coupon_id_array[$i],
                                        );

                    //Confirm the record is success
//                    $this->db->insert('cou_return', $value_array);
                    array_push($overall_array_to_store, $value_array);
                }

                //Check whether the coupon number is in range
                $start = $coupon_result[0]->sequence_start_digit;
                $end = $coupon_result[0]->sequence_end_digit;

                $year = 'all';
                $expiry_report_query = $this->log_expiry_date_model->get_by_coupon_name($coupon_type_uid, $year);

                $in_range = false;
                if ($expiry_report_query->num_rows() > 0){
                    $result_set = $expiry_report_query->result();

                    foreach ($result_set as $row){
                        //check if it is in range
                        $expiry_date_sequence_range = $row->expiry_date_sequence_range;

                        $pos = strrpos($expiry_date_sequence_range, "/");
                        if ($pos === false) { // note: three equal signs
                            // not found...
                            $sequence_start = $expiry_date_sequence_range;
                            $sequence_end = $expiry_date_sequence_range;
                        } else {
                            $pieces = explode("/", $expiry_date_sequence_range);
                            $sequence_start = $pieces[0];
                            $sequence_end = $pieces[1];
                        }

                        $length = (int) $end - (int) $start + 1;
                        $sequence_start_substring = substr($sequence_start, $start-1, $length);
                        $sequence_end_substring = substr($sequence_end, $start-1, $length);

                        $coupon_id_substring = substr($coupon_id_array[$i], $start-1, $length);

                        if ((int)$coupon_id_substring <= (int) $sequence_end_substring
                                && (int)$coupon_id_substring >= (int) $sequence_start_substring){
                            $in_range = true;
                        }
                    }
                }

                if (!$in_range){
                    //Log
                    $content=" [Fault] $coupon_name Coupon No.".$coupon_id_array[$i]." Does not exist.";
                    $category="coupon_error";
                    $this->log_model->update_log($content,$category);
                    
                    //Still success in this case, but there will be a log on admin side
                }
                
                $current_user = $this->user_model->get_by_primary_key($se_user_uid);
                $current_user_nick_name = $current_user[0]->user_nick_name;
                
                $log_string = "[Return] $current_user_nick_name return $coupon_name with quantity $assign_quantity";
                
                array_push($success_log_string_array, $log_string);
                //$this->coupon_model->log_log($log_string, "receive_distribute", $target_user_uid);

                if ($success){
                    $success_quantity++;//count the number of eventually returned coupon
                    array_push($success_array, "true");
                } else {
                    array_push($success_array, (string)$error_code);
                }
            } else {
                $success = false;
                array_push($success_array, "[Error]No enough onhand coupon");
            }
//            $overall_success = $overall_success && $success;
        }
        
        //Check if all success
//        if ($overall_success){
            //Insert all the record
            foreach ($overall_array_to_store as $return_data){
                $this->db->insert('cou_return', $return_data);
            }
            foreach ($success_log_string_array as $success_log_string){
                $this->coupon_model->log_log($success_log_string, "receive_distribute", $target_user_uid);
            }
            
            $coupon_onhand_result = $this->coupon_onhand_model->get_by_coupon_user_uid($coupon_type_uid, $se_user_uid);
            $coupon_onhand_result_row = $coupon_onhand_result->result();
            
//            $total_quantity = sizeof($coupon_id_array);
            if (sizeof($coupon_onhand_result_row)>0){
                $coupon_onhand_record = $coupon_onhand_result_row[0];
                $coupon_onhand_uid = $coupon_onhand_record->coupon_onhand_uid;

                //minus the quantity from coupon onhand
                $result = $this->coupon_onhand_model->get_by_primary_key($coupon_onhand_uid);

                //Add it to the receive, so that the AM saw the return
                $t=time();
                $today = date("Y-m-d H:i:s",$t);
                
                if($success_quantity > 0){
                    $new_model = new Coupon_receive_model();
                    $new_model->set_value("distributer_uid", $se_user_uid);
                    $new_model->set_value("receiver_uid", $target_user_uid);
                    $new_model->set_value("coupon_type_uid", $result[0]->coupon_type_uid);
                    $new_model->set_value("quantity", $success_quantity);//count the number of eventually returned coupon
                    $new_model->set_value("distribute_date", $today);
                    $new_model->set_value("status", "return");
                    $new_model->set_value("coupon_onhand_uid", $coupon_onhand_uid);

                    $this->coupon_receive_model->insert($new_model);
                }
            }
//        }
        
        echo json_encode($success_array);
    }
    
    //get coupon type according to user_role_id stored when login 
    public function get_coupon_type(){
        $stack = array();
        if($this->user_model->checked_if_login()){
            $role_id = $this->usermodule->get_user_role();
            $stack=$this->model_object->find_coupon_type($role_id);
            echo json_encode($stack);
        }
        else{
            echo null;
        }
    }
}