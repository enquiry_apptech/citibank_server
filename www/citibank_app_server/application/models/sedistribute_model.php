<?php
class Sedistribute_model extends CI_Model {
    
    var $table_application = 'app_application';
    var $table_coupon_used = 'app_app_coupon';
    var $table_coupon_type = 'cou_coupon_type';
    var $table_application_type='application_type';
    var $table_coupon_onhand = 'cou_coupon_onhand';
       
    const model_log='log_model';
    
    
    function __construct() {
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
        $this->load->library('userrole/UserModule');
        $this->load->model($this::model_log);
    }
    
    
    //check whether the username is equal to the password 
    function if_password_not_changed($username, $password){
        $success=true;
        if(md5($username)==md5($password)){
            $success=false;
        }
        return $success;
    }
	
	function sequence_checking($coupon_type_uid, $scan_or_input_coupon_number) {
        //Find the stocked in coupons sequences of the coupon type
        $user_uid = 1; //Sam account's user uid
        
        //Selec all the coupon ohhand of SAM of particular coupon type
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        $this->db->where('coupon_onhand.coupon_type_uid', $coupon_type_uid);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            //Check whether it matches any sequence
            $sam_onhand_coupon_result = $query->result();
            
            //For each result, check if the sequence is inside
            foreach($sam_onhand_coupon_result as $onhand_result){
                $sequence_start = $onhand_result->sequence_start;
                $sequence_end = $onhand_result->sequence_end;
                $start = $onhand_result->sequence_start_digit;
                $end = $onhand_result->sequence_end_digit;

                $length = (int) $end - (int) $start + 1;
                $sequence_substring = substr($scan_or_input_coupon_number, $start-1, $length);
                $sequence_start_substring = substr($sequence_start, $start-1, $length);
                $sequence_end_substring = substr($sequence_end, $start-1, $length);
            
                $success = false;
                //Check if it is a number
                if (!is_numeric($sequence_substring)){
//                    echo "format_error";
                    return false;
                } else if ((int)$sequence_substring >= (int)$sequence_start_substring && (int)$sequence_substring <= (int)$sequence_end_substring){
//                    echo "success";
                    $success = true;
                    return true;
                }
            }
            return $success;
        } else {
            return false;
        }
    }
    
    //to check whether this application id has been used
    function if_appid_used($application_id) {
        $used=false;
		$this->db->select('app_id');
        $this->db->from('app_application');
        $this->db->where('app_id', $application_id);
        $query_s = $this->db->get();
        if ($query_s->num_rows() > 0) {
            $used=true;
			$category="coupon_error";
            $content="[Duplicate]"." Application No. ".$application_id."has already been used.";
            
            //Cancel this due to request from client
            //$this->log_model->update_log($content,$category);
        }	
        return $used;   
    }
    
    //store application id and the related salesman seoid
    function store_application_id($application_id, $phone_number){
        $stored=false;
        
	$this->session->set_userdata('application_id',$application_id);
        $user_uid=$this->usermodule->get_user_role();
        $new_uid_result = $this->uidgenerator->request_uid($this->table_application);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        $date = date('Y-m-d H:i:s');
        $data = array(
            'application_uid' => $new_uid ,
            'user_uid' => $user_uid,
            'app_id' => $application_id,
            'app_create_date' => $date,
            'phone_number' => $phone_number);
        
        $this->db->insert('app_application', $data);
		$stored=true;
		return $stored;
    }
    
    //to check whether this coupon_id bar scanned result has been used
    function if_coupid_used($coupon_id){
        $used=false;
		$user_uid=$this->usermodule->get_user_role();
        $query = $this->db->get_where('app_app_coupon', array('coupon_number' => $coupon_id));
        if ($query->num_rows() > 0){
		if($coupon_id!=NULL){
            $used=true;
            $content="[Duplicate] Coupon No:" . $coupon_id . "has already been used.";
			$category="coupon_error";
            $this->log_model->update_log($content,$category);
        }
		}
        return $used;
    }
    //These two functions only used when checking the offline upload
	//to check whether this application id has been used
    function offline_if_appid_used($application_id,$user_login_id) {
        $used=false;
		$this->db->select('app_id');
        $this->db->from('app_application');
        $this->db->where('app_id', $application_id);
        $query_s = $this->db->get();
        if ($query_s->num_rows() > 0) {
            $used=true;
			$category="offline_upload_warning";
            $content="[Duplicate]"." Application No. ".$application_id."has already been used. Offline Uploaded by User ".$user_login_id;
            $this->log_model->update_log($content,$category);
        }	
        return $used;   
    }
	//to check whether this coupon_id bar scanned result has been used
    function offline_if_coupid_used($coupon_id,$user_login_id){
        $used=false;
		$user_uid=$this->usermodule->get_user_role();
        $query = $this->db->get_where('app_app_coupon', array('coupon_number' => $coupon_id));
        if ($query->num_rows() > 0){
		if($coupon_id!=NULL){
            $used=true;
            $content="[Duplicate] Coupon No:" . $coupon_id . "has already been used. Offline Uploaded by User ".$user_login_id;
			$category="offline_upload_warning";
            $this->log_model->update_log($content,$category);
        }
		}
        return $used;
    }
	
	
    //this place should write a function to check whether it is in the coupon list
    //coupon is generated using a series of numbers
    //undone
    
    //used for set application_id
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    //search coupon's info based on its quantity ? > 0 for checking on At->Ct
    function search_coupon_info($app_type_id){
	    $end_date;
	    $start_date;
        //search coupon_type_id according to app_type_id in cou_offer_update table
        $coupon_type_uid;
        $stack = array();
        $quantity = -1;
        $user_uid=$this->usermodule->get_user_role();
        $query = $this->db->get_where('cou_offer_update', array('application_type_uid' => $app_type_id));
        foreach ($query->result() as $row){
            $coupon_type_uid = $row->coupon_type_uid;
			$this->db->select('start_date, end_date');
            $this->db->from('cou_offer_update');
            $this->db->where('coupon_type_uid', $coupon_type_uid);	
            $this->db->where('application_type_uid', $app_type_id);			
            $query_date = $this->db->get();
             if ($query_date->num_rows() > 0){
            foreach ($query_date->result() as $row){
                $start_date = $row->start_date;
                $end_date = $row->end_date;
            }
            }
            
            //get quantity according to the coupon_type_uid
            $this->db->select('quantity');
            $this->db->from('cou_coupon_onhand');
			$this->db->where('coupon_type_uid', $coupon_type_uid);
			
			$this->db->where('user_uid', $user_uid);
            $query_quantity = $this->db->get();
            if ($query_quantity->num_rows() > 0){
            foreach ($query_quantity->result() as $row){
                $quantity = $row->quantity;
            }
            }
			else{
			$quantity=0;
			}
            if($quantity > 0){
			
            //search coupon information based on the coupon_type_uid   
            $this->db->select('value,name,scanning_instruction,scanable');
            $this->db->from('cou_coupon_type');
            $this->db->where('coupon_type_uid', $coupon_type_uid); 
               			
            $query_coupon_type = $this->db->get();
           
            if ($query_coupon_type->num_rows() > 0){
			    date_default_timezone_set('Asia/Hong_Kong');
				$format = 'Y-m-d H:i:s';
                $date = DateTime::createFromFormat($format, $end_date);
				$interval = date_diff(new DateTime(), $date);
		        //if (new DateTime() < new DateTime($end_date)){
				$start_datetime = DateTime::createFromFormat($format, $start_date);
                    $interval_start = date_diff(new DateTime(), $start_datetime);
                    
                    if ($interval->invert == 0 && $interval_start->invert == 1) {
                foreach ($query_coupon_type->result() as $row){
                        $data = array(
                            "coupon_type_uid" => $coupon_type_uid,
                            "value" =>$row->value ,
                            "name" => $row->name,
                            'scanning_instruction' => $row->scanning_instruction,
                            "scanable" => $row->scanable);
                        array_push($stack, $data);   
                    }
                  }
				}
            }
			else{
                        
                    }
        }    
        return $stack;
    }
    
    //used in determine whether transaction is successfully or not?
    function store_used_coupon($application_id, $app_type_id, $coupon_id,$coupon_type_id){
        $success  = true;
        $quantity = 0;
        $user_uid = $this->usermodule->get_user_role();
        //search application uid
        $application_uid = 0;
        $query = $this->db->get_where('app_application', array('app_id' => $application_id));
        foreach ($query->result() as $row){
            $application_uid = $row->application_uid;
        }
         
        //inserting coupon used information in app_app_coupon table
        $new_uid_result_coupon_used = $this->uidgenerator->request_uid($this->table_coupon_used);
        $new_uid_coupon_used = $new_uid_result_coupon_used[0]->uid_gen_current_uid;
        $coupon_used_data=array(
            'app_coupon_uid'  => $new_uid_coupon_used ,
			'app_type_uid' => $app_type_id,
            'application_uid' => $application_uid ,
            'coupon_type_uid' => $coupon_type_id,
            'coupon_number'  => $coupon_id);
        $this->db->insert('app_app_coupon', $coupon_used_data);
        
        
        //change cou_coupon_type quantity
        $this->db->select('quantity');
        $this->db->from('cou_coupon_onhand');
        $this->db->where('user_uid', $user_uid); 
        $this->db->where('coupon_type_uid', $coupon_type_id); 
        $query_quantity = $this->db->get();
        if ($query_quantity->num_rows() > 0){
            foreach ($query_quantity->result() as $row){
                $quantity=$row->quantity;
            }
        }
        else{
            
        }
        if($quantity!=0){
            $quantity=$quantity-1;
        }
        $data=array(
            'quantity' => $quantity
        );
        $this->db->where('user_uid', $user_uid);
        $this->db->where('coupon_type_uid', $coupon_type_id);
        $this->db->update('cou_coupon_onhand', $data);
        
        return $success;
    }
    
    //search the database about all the coupon information based on 
    //the application id, used in the listall, final step
    function list_all_coupon($app_id){
        $this->db->select('cou_coupon_type.coupon_type_uid,value,name,app_app_coupon.coupon_number');
        $this->db->from('cou_coupon_type');
        $this->db->join('app_app_coupon', 'app_app_coupon.coupon_type_uid = cou_coupon_type.coupon_type_uid' , 'left');
        $this->db->join('app_application', 'app_application.application_uid = app_app_coupon.application_uid' , 'left');
        $this->db->where('app_application.app_id', $app_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return null;
        }
    }
	
	//new file 
	function find_coupon_add($role_id){
        $stack = array();
        $this->db->select('cou_coupon_type.name, cou_coupon_onhand.quantity, cou_coupon_type.value');
        $this->db->from('cou_coupon_type');
        $this->db->join('cou_coupon_onhand', 'cou_coupon_onhand.coupon_type_uid = cou_coupon_type.coupon_type_uid' , 'left');
        $this->db->where('cou_coupon_onhand.user_uid', $role_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                if($row->quantity > 0){
                    $data = array("name" => $row->name,
								 "quantity" => $row->quantity,
								 "value" => $row->value);
                    array_push($stack, $data);
                }
            }
        }
        else{
        }  
        return $stack;
    }
    
}
