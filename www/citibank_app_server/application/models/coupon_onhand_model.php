<?php

class Coupon_onhand_model extends CI_Model {
    var $coupon_onhand_uid = '';
    var $user_uid = '';
    var $coupon_type_uid = '';
    var $quantity = '';
//    var $sequence_start = '';
//    var $sequence_end = '';
//    var $expiry_date = '';
    
    var $table_name = 'cou_coupon_onhand';
    
    const role_table_name = 'usr_role';
    const userrole_table_name = 'usr_userrole';
    
    const allow_edit = false;
    const allow_delete = false;
    const uid_field_name = 'coupon_onhand_uid';
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
    }
    
    function get_by_primary_key($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('coupon_onhand_uid' => $key));
        return $query->result();
    }
    
    function get_by_coupon_type_uid($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('coupon_type_uid' => $key));
        return $query;
    }
    
    function get_by_user_uid($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('user_uid' => $key));
        return $query;
    }
    
    function get_by_user_uid_for_expiry_report($user_uid) {
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_by_coupon_user_uid_for_expiry_report($coupon_type_uid, $user_uid) {
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        $this->db->where('coupon_onhand.coupon_type_uid', $coupon_type_uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_by_coupon_user_uid($coupon_type_uid, $user_uid) {
        $query = $this->db->get_where($this->table_name, array('coupon_type_uid' => $coupon_type_uid, 'user_uid' => $user_uid));
        return $query;
    }
    
    function get_all_for_coupon_distribution($user_uid) {
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_for_coupon_distribution_add($coupon_onhand_uid) {
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.coupon_onhand_uid', $coupon_onhand_uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Basic Model Operation
     */
    function values(){
        $value_array = array(
            "coupon_onhand_uid" => $this->coupon_onhand_uid,
            "user_uid" => $this->user_uid,
            "coupon_type_uid" => $this->coupon_type_uid,
            "quantity" => $this->quantity
//            "sequence_start" => $this->sequence_start,
//            "sequence_end" => $this->sequence_end,
//            "expiry_date" => $this->expiry_date
        );
        
        return $value_array;
    }
    
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    /*
     * Basic DB Operation
     */
    function insert($new_model) {
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this->table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $new_model->set_value($this::uid_field_name, $new_uid);
        $this->db->insert($this->table_name, $new_model->values());
        
        return $new_uid;
    }
    
    function update($model) {
        $new_array = $model->values();

        $uid_field_name = $this::uid_field_name;
        $uid = $model->$uid_field_name;
        $this->db->where($this::uid_field_name, $uid);
        $this->db->update($this->table_name, $new_array);
    }
    
}