<?php

class Coupon_receive_model extends CI_Model {
    var $coupon_receive_uid = '';
    var $distributer_uid = '';
    var $receiver_uid = '';
    var $coupon_onhnad_uid = '';
    var $coupon_type_uid = '';
    var $quantity = '';
    var $distribute_date = '';
    var $status = '';
    
    var $table_name = 'cou_coupon_receive';
    
    const role_table_name = 'usr_role';
    const userrole_table_name = 'usr_userrole';
    
    const allow_edit = false;
    const allow_delete = false;
    const uid_field_name = 'coupon_receive_uid';
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
    }
    
    function get_by_primary_key($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array($this::uid_field_name => $key));
        return $query;
    }
    
    function get_by_user_uid($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('receiver_uid' => $key));
        return $query;
    }
    
    function get_by_distributer_uid_coupon_type_uid($user_uid, $coupon_type_uid) {
        $this->db->select('coupon_receive.*');
        $this->db->from('cou_coupon_receive coupon_receive');
        $this->db->where('distributer_uid', $user_uid);
        $this->db->where('coupon_type_uid', $coupon_type_uid);
        
        $this->db->where('status', 'distribution');//fix bug
        
        $query = $this->db->get();
        return $query;
    }
    
    function get_by_user_uid_for_receive_record($uid, $role_name) {
        $this->db->select('coupon_receive.*, distributer.user_nick_name distributer, manager.manager_uid, manager.team manager_manager_team, am_manager.team am_manager_team, se_am_manager.team se_manager_team, am.am_uid, se.se_uid, coupon_type.name coupon_name');
        $this->db->from('cou_coupon_receive coupon_receive');
        
        $where = "receiver_uid = '$uid'";
        $where .= "AND (status='return' OR status='distribution')";
//        $this->db->where('receiver_uid', $uid);
//        $this->db->where('status');
        $this->db->where($where, NULL, FALSE);
        $this->db->join('usr_user distributer', 'distributer.user_uid = coupon_receive.distributer_uid' , 'left');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_receive.coupon_type_uid' , 'left');
        
//        if ($role_name=="super"){
            
//        } else if ($role_name=="manager") {
            $this->db->join('rol_manager manager', 'manager.user_uid = coupon_receive.distributer_uid' , 'left');
            
            $this->db->join('rol_am am', 'am.user_uid = coupon_receive.distributer_uid' , 'left');
            $this->db->join('rol_manager am_manager', 'am.manager_uid = am_manager.manager_uid' , 'left');
            
            $this->db->join('rol_se se', 'se.user_uid = coupon_receive.distributer_uid' , 'left');
            $this->db->join('rol_am se_am', 'se.am_uid = se_am.am_uid' , 'left');
            $this->db->join('rol_manager se_am_manager', 'se_am.manager_uid = se_am_manager.manager_uid' , 'left');
//        } else if ($role_name=="am") {
            
//        } else if ($role_name=="se") {
            
//        }
        
        
        $this->db->order_by('coupon_receive.distribute_date', 'desc');
            
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_by_user_uid_for_receive_record_history($uid, $role_name, $month) {
        $this->db->select('coupon_receive.*, distributer.user_nick_name distributer,receiver.user_nick_name receiver, manager.manager_uid, manager.team manager_manager_team, am_manager.team am_manager_team, se_am_manager.team se_manager_team, am.am_uid, se.se_uid, coupon_type.name coupon_name');
        $this->db->from('cou_coupon_receive coupon_receive');
        
        $where = "(coupon_receive.receiver_uid = '$uid'";
        $where .= " OR coupon_receive.distributer_uid = '$uid')";
        if ($month!='all'){
            $where .= " and coupon_receive.distribute_date like '$month%'";
        }
        $where .= "AND (status='return_close' OR status='distribution_close' OR status='return_reject' OR status='distribution_reject')";
        
        
        
        $this->db->where($where, NULL, FALSE);
        $this->db->join('usr_user distributer', 'distributer.user_uid = coupon_receive.distributer_uid' , 'left');
        $this->db->join('usr_user receiver', 'receiver.user_uid = coupon_receive.receiver_uid' , 'left');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_receive.coupon_type_uid' , 'left');
        
        $this->db->join('rol_manager manager', 'manager.user_uid = coupon_receive.distributer_uid' , 'left');

        $this->db->join('rol_am am', 'am.user_uid = coupon_receive.distributer_uid' , 'left');
        $this->db->join('rol_manager am_manager', 'am.manager_uid = am_manager.manager_uid' , 'left');

        $this->db->join('rol_se se', 'se.user_uid = coupon_receive.distributer_uid' , 'left');
        $this->db->join('rol_am se_am', 'se.am_uid = se_am.am_uid' , 'left');
        $this->db->join('rol_manager se_am_manager', 'se_am.manager_uid = se_am_manager.manager_uid' , 'left');
        
        $this->db->order_by('coupon_receive.distribute_date', 'desc');
        
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Basic Model Operation
     */
    function values(){
        $value_array = array(
            "coupon_receive_uid" => $this->coupon_receive_uid,
            "distributer_uid" => $this->distributer_uid,
            "receiver_uid" => $this->receiver_uid,
            "coupon_onhand_uid" => $this->coupon_onhand_uid,
            "coupon_type_uid" => $this->coupon_type_uid,
            "quantity" => $this->quantity,
            "distribute_date" => $this->distribute_date,
            "status" => $this->status
        );
        
        return $value_array;
    }
    
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    /*
     * Basic DB Operation
     */
    function insert($new_model) {
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this->table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $new_model->set_value($this::uid_field_name, $new_uid);
        $this->db->insert($this->table_name, $new_model->values());
        
        return $new_uid;
    }
    
    function close_receive($uid){
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array($this::uid_field_name => $key));
        $result = $query->result();
        $row = $result[0];
        $new_status = $row->status."_close";
        
        $this->db->flush_cache();
        
        $new_array = ["status"=>$new_status];
        
        $this->db->where("coupon_receive_uid", $uid);
        
        $this->db->update("cou_coupon_receive", $new_array);
    }
    
    function reject_receive($uid){
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array($this::uid_field_name => $key));
        $result = $query->result();
        $row = $result[0];
        $new_status = $row->status."_reject";
        
        $this->db->flush_cache();
        
        $new_array = ["status"=>$new_status];
        
        $this->db->where("coupon_receive_uid", $uid);
        
        $this->db->update("cou_coupon_receive", $new_array);
    }
    
//    function delete($uid){
//        $uid_delete=(int)$uid;
//        $this->db->delete($this->table_name, array($this::uid_field_name => $uid_delete));
//    }
}