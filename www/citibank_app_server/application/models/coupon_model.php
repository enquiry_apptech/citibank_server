<?php

class Coupon_model extends CI_Model {

    /*
     * Columns and table name
     */
    var $coupon_type_uid = '';
    var $value = '';
    var $name = '';
//    var $app_type_uid = '';
//    var $quantity = '';
    var $scanable = '';
    var $sequence_start_digit = '';
    var $sequence_end_digit = '';
    
    var $table_name = 'cou_coupon_type';

    const role_table_name = 'usr_role';
    const userrole_table_name = 'usr_userrole';
    
    const allow_edit = true;
    const allow_delete = true;
    const uid_field_name = 'coupon_type_uid';
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
    }
    
    function get_by_primary_key($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array($this::uid_field_name => $key));
        return $query->result();
    }
    
    function get_all($offset=-1) {
        $this->db->select('coupon_type.*');
        $this->db->from('cou_coupon_type coupon_type');
        
        if ($offset>=0){
            $this->db->limit(30, $offset);
        }
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_query() {
        $this->db->select('coupon_type.*');
        $this->db->from('cou_coupon_type coupon_type');
        
        $query = $this->db->get();
        return $query;
    }
    
    function get_all_for_admin() {
        $this->db->select('coupon_type.*, CONCAT("From ", coupon_type.sequence_start_digit, "-", coupon_type.sequence_end_digit, " digits") effective_sequence', FALSE);
        $this->db->from('cou_coupon_type coupon_type');

        $query = $this->db->get();
        return $query->result();
    }
    
    function get_sequence_digits_by_coupon_type_uid($uid) {
        $this->db->select('name, sequence_start_digit, sequence_end_digit');
        $this->db->from('cou_coupon_type coupon_type');
        $this->db->where($this::uid_field_name, $uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_movement($user_uid){
        $this->db->select('*');
        $this->db->from('log_movement movement');
        $this->db->where('from_user_uid', $user_uid);
        $this->db->or_where('to_user_uid', $user_uid);
        $this->db->order_by('create_date', 'desc');
        
        $query = $this->db->get();
        return $query;
    }
    
    function get_movement_for_report($user_uid, $coupon_type_uid, $month, $from_user_uid, $to_user_uid, $offset=-1){
        $this->db->select('*');
        $this->db->from('log_movement movement');
        
        $where = "1=1";
        
        if ($coupon_type_uid!='all'){
            $where .= " and movement.coupon_type_uid = '$coupon_type_uid'";
        }
        
        if ($month!='all'){
            $where .= " and movement.create_date like '$month%'";
        }
        
        $from_user_uid_string = '';
        $to_user_uid_string = '';
        
        if ($from_user_uid=='all') {
            $from_user_uid_string = 'all';
        } else {
            $from_user_uid_string = $from_user_uid;
        }
        
        if ($to_user_uid=='all') {
            $to_user_uid_string = 'all';
        } else {
            $to_user_uid_string = $to_user_uid;
        }
        
        if ($from_user_uid=='all') {
            
        } else {
            $where .= " and movement.from_user_uid='$from_user_uid_string'";
        }
        
        if ($to_user_uid=='all') {
            
        } else {
            $where .= " and movement.to_user_uid='$to_user_uid_string'";
        }
//        if ($from_user_uid=='all' || $to_user_uid=='all') {
//            $where .= " and (movement.from_user_uid='$from_user_uid_string' OR movement.to_user_uid='$to_user_uid_string')";
//        } else {
//            $where .= " and (movement.from_user_uid='$from_user_uid_string' AND movement.to_user_uid='$to_user_uid_string')";
//        }
        
        $this->db->where($where, NULL, FALSE);
        
        $this->db->order_by('create_date', 'desc');
        
        if ($offset>=0){
            $this->db->limit(30, $offset);
        }
        
        $query = $this->db->get();
        return $query;
    }
    
    function get_listall_field_setting(){
        /*
         * Setup this array, then you can control the display of the column
         * control the type of column
         */
        $setting = array(
            "name" => array( "display" => true, "type" => "text"),
            "value" => array("display" => true, "type" => "text"),
            "effective_sequence" => array("display" => true, "type" => "text"),
            "scanning_instruction" => array("display" => true, "type" => "text"),
            "coupon_type_uid" => array("display" => false, "type" => "text"),
//            "app_type_uid" => array("display" => false, "type" => "text"),
//            "quantity" => array("display" => false, "type" => "text"),
            "scanable" => array("display" => true, "type" => "boolean")
        );
        
        return $setting;
    }
    
    function get_create_field_setting(){
        //Roles in the project
        $option_array = array(
            array("option_name"=>"Manager", "option_value"=>"manager", "selected"=>true, "onselect"=>""),
            array("option_name"=>"AM", "option_value"=>"am", "selected"=>false, "onselect"=>"window.location.href='".site_url('admin_platform/am/create')."';"),
            array("option_name"=>"SE", "option_value"=>"se", "selected"=>false, "onselect"=>"window.location.href='".site_url('admin_platform/se/create')."';")
        );
        $object = json_decode(json_encode($option_array), FALSE);
        
        $setting = array(
            "name" => array("display" => true, "type" => "text", "required" => true),
            "value" => array("display" => true, "type" => "text", "required" => true),
            "scanning_instruction" => array("display" => true, "type" => "text", "required" => true),
            "coupon_type_uid" => array("display" => false, "type" => "dropdown", "required" => true, "list"=>$object),
//            "app_type_uid" => array("display" => true, "type" => "text", "required" => true),
//            "quantity" => array("display" => false, "type" => "text", "required" => true),
            "scanable" => array("display" => true, "type" => "boolean", "required" => true)
        );
        
        return $setting;
    }
    
    function get_field_translation($language){
        if ($language=="tc"){
            $coupon_type_uid = 'Coupon ID';
            $value = 'Value';
            $name = 'Name';
//            $app_type_uid = 'App Type';
//            $quantity = 'Quantity';
            $scanning_instruction = 'Scanning Instruction';
            $scanable = 'Scanable';
        } else {
            $coupon_type_uid = 'Coupon ID';
            $value = 'Coupon Value';
            $effective_sequence = 'Effective sequence';
            $name = 'Coupon Name';
//            $app_type_uid = 'App Type';
//            $quantity = 'Quantity';
            $scanning_instruction = 'Scanning Instruction';
            $scanable = 'Scanable';
        }

        $translation_array = array(
            "coupon_type_uid" => array("value" => $coupon_type_uid),
            "value" => array("value" => $value),
            "effective_sequence" => array("value" => $effective_sequence),
            "name" => array("value" => $name),
//            "app_type_uid" => array("value" => $app_type_uid),
//            "quantity" => array("value" => $quantity),
            "scanning_instruction" => array("value" => $scanning_instruction),
            "scanable" => array("value" => $scanable)
        );

        return $translation_array;
    }
    
    /*
     * Basic Model Operation
     */
    function values(){
        $value_array = array(
            "coupon_type_uid" => $this->coupon_type_uid,
            "value" => $this->value,
            "name" => $this->name,
//            "app_type_uid" => $this->app_type_uid,
//            "quantity" => $this->quantity,
            "scanning_instruction" => $this->scanning_instruction,
            "scanable" => $this->scanable,
            "sequence_start_digit" => $this->sequence_start_digit,
            "sequence_end_digit" => $this->sequence_end_digit
        );
        
        return $value_array;
    }
    
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    function log_movement($str, $coupon_type_name, $coupon_type_uid, $inout, $quantity, $from_user_name, $to_user_name, $from_user_uid, $to_user_uid){
        $new_uid_result = $this->uidgenerator->request_uid('log_movement');
        $movement_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        date_default_timezone_set('Asia/Hong_Kong');
        $t=time();
        $create_date = date("Y-m-d H:i:s",$t);
        
        $value = array("from_user_uid"=>$from_user_uid, "to_user_uid"=>$to_user_uid, "stock_inout_quantity"=>$quantity, "coupon_type_uid"=>$coupon_type_uid, "coupon_type_name"=>$coupon_type_name, "from_user_name"=>$from_user_name, "to_user_name"=>$to_user_name, "movement_uid"=>$movement_uid, "movement_desc"=>$str, "create_date"=>$create_date);
        
        
        $this->db->insert('log_movement', $value);
    }
    
    function check_if_expiry_log_exist($logs){
        $this->db->select('*');
        $this->db->from('log');
        $this->db->where('logs', $logs);
        
        $query = $this->db->get();
        
        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    function log_log($log, $category, $user_uid) {
        $new_uid_result = $this->uidgenerator->request_uid('log');
        $uid = $new_uid_result[0]->uid_gen_current_uid;
        
        date_default_timezone_set('Asia/Hong_Kong');
        $t=time();
        $create_date = date("Y-m-d H:i:s",$t);
        
        $value = array("uid"=>$uid, "logs"=>$log, "time"=>$create_date, "category"=>$category, "user_uid"=>$user_uid);
        
        $this->db->insert('log', $value);
    }
    
    /*
     * Basic DB Operation
     */
    function insert($new_model) {
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this->table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $new_model->set_value($this::uid_field_name, $new_uid);
        $this->db->insert('cou_coupon_type', $new_model->values());
        
        return $new_uid;
    }
    
    function insert_user_role($user_uid, $role_uid){
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this::userrole_table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $value_array = array("usr_userrole_uid"=>$new_uid,"user_uid"=>$user_uid,"role_uid"=>$role_uid);
        $this->db->insert($this::userrole_table_name, $value_array);
        
        return $new_uid;
    }

    function delete($uid){
        $uid_delete=(int)$uid;
        $this->db->delete($this->table_name, array($this::uid_field_name => $uid_delete));
    }
    
    function delete_log($uid){
        $uid_delete=(int)$uid;
        $this->db->delete('log', array('uid' => $uid_delete));
    }
    
    function delete_userrole($uid){
        $uid_delete=(int)$uid;
        $this->db->delete('usr_userrole', array('user_uid' => $uid_delete));
    }
    
     function update($model){
        $new_array = $model->values();
        
        $uid_field_name = $this::uid_field_name;
        $uid = $model->$uid_field_name;
            $this->db->where($this::uid_field_name, $uid);
           $this->db->update($this->table_name, $new_array);

    }
}
?>