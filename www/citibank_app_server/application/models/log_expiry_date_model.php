<?php

class Log_expiry_date_model extends CI_Model {
    
    var $log_expiry_date_uid = '';
    var $expiry_date_coupon_type_uid = '';
    var $expiry_date_coupon_name = '';
    var $expiry_date_sequence_range = '';
    var $quantity = '';
    var $expiry_date = '';
    
    var $table_name = 'log_expiry_date';
    const uid_field_name = 'log_expiry_date_uid';

    function get_all() {
        $this->db->select('expiry_date.*');
        $this->db->from('log_expiry_date expiry_date');
        $this->db->order_by('expiry_date', 'desc');
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_by_coupon_name($coupon_type_uid, $year, $offset=-1) {
        $this->db->select('*');
        $this->db->from('log_expiry_date expiry_date');
        
        $where = "1=1";
        
        if ($coupon_type_uid!='all'){
            $where .= " and expiry_date.expiry_date_coupon_type_uid = $coupon_type_uid";
        }
        
        if ($year!='all'){
            $where .= " and expiry_date.expiry_date like '$year%'";
        }
        
        if ($offset>=0){
            $this->db->limit(30, $offset);
        }
        
        $this->db->where($where, NULL, FALSE);
        
        $this->db->order_by('expiry_date', 'desc');
        
        $query = $this->db->get();
        return $query;
    }
    
    
    
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    function values() {
         $value_array = array(
             "log_expiry_date_uid" => $this->log_expiry_date_uid,
             "expiry_date_coupon_type_uid" => $this->expiry_date_coupon_type_uid,
             "expiry_date_coupon_name" => $this->expiry_date_coupon_name,
             "expiry_date_sequence_range" => $this->expiry_date_sequence_range,
             "quantity" => $this->quantity,
             "expiry_date" => $this->expiry_date
         );

         return $value_array;
     }
     
     /*
      * Basic DB Operation
      */

     function insert($new_model) {
         //Request a new uid
         $new_uid_result = $this->uidgenerator->request_uid($this->table_name);
         $new_uid = $new_uid_result[0]->uid_gen_current_uid;
         $new_model->set_value($this::uid_field_name, $new_uid);
         $this->db->insert($this->table_name, $new_model->values());
         return $new_uid;
     }
}