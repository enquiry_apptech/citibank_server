<?php
date_default_timezone_set('Asia/Hong_Kong');

class Sereturn_model extends CI_Model {
    
    var $table_application = 'cou_coupon_receive';
    var $table_coupon_onhand = 'cou_coupon_onhand';
    
    const model_log='log_model';
    
    
    function __construct() {
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
        $this->load->model($this::model_log);
        $this->load->library('userrole/UserModule');
    }
    
    //find coupon type using role id
    function find_coupon_type($role_id){
        $stack = array();
        $this->db->select('cou_coupon_type.coupon_type_uid, cou_coupon_type.name, cou_coupon_onhand.quantity, cou_coupon_type.scanning_instruction, cou_coupon_type.scanable');
        $this->db->from('cou_coupon_type');
        $this->db->join('cou_coupon_onhand', 'cou_coupon_onhand.coupon_type_uid = cou_coupon_type.coupon_type_uid' , 'left');
        $this->db->where('cou_coupon_onhand.user_uid', $role_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                if($row->quantity > 0){
                    $data = array("name" => $row->name,
                                  "coupon_type_uid" => $row->coupon_type_uid,
								  "scanning_instruction"=>$row->scanning_instruction,
								  "scanable"=>$row->scanable);
                    array_push($stack, $data);
                }
            }
        }
        else{
        }  
        return $stack;
    }
   
    //find the am_id according to user_id
    function find_related_am($user_role_id){
	    $am_uid=0;
        $this->db->select('am_uid');
        $this->db->from('rol_se');
        $this->db->where('user_uid', $user_role_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $am_uid = $row->am_uid;
            }
        }
        else{   
        }
		return $am_uid;
    }
    
    function return_coupon($user_id, $am_id, $coupon_type_id,$coupon_id){
	    $success=true;
        $coupon_onhand_uid = 0;
        $quantity=1;
        $status="return";
        $date=date('Y-m-d H:i:s');
        $am_role=0;
        //get the new uid 
        $new_uid_result = $this->uidgenerator->request_uid($this->table_application);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        //get am role
        $this->db->select('manager_uid');
        $this->db->from('rol_am');
        $this->db->where('am_uid', $am_id); 
        $query_role = $this->db->get();
        if ($query_role->num_rows() > 0){
            foreach ($query_role->result() as $row){
                $am_role = $row->manager_uid;
            }
        }
        else{
        }
        //select coupon_onhand_uid
        $this->db->select('coupon_onhand_uid');
        $this->db->from('cou_coupon_onhand');
        $this->db->where('user_uid', $user_id); 
        $query_coupon_onhand = $this->db->get();
            foreach ($query_coupon_onhand->result() as $row){
                $coupon_onhand_uid = $row->coupon_onhand_uid;
            }
            
       
        	
		 //change cou_coupon_type quantity
        $this->db->select('quantity');
        $this->db->from('cou_coupon_onhand');
        $this->db->where('user_uid', $user_id); 
        $this->db->where('coupon_type_uid', $coupon_type_id); 
        $query_quantity = $this->db->get();
        if ($query_quantity->num_rows() > 0){
            foreach ($query_quantity->result() as $row){
                $quantity=$row->quantity;
            }
        }
        else{
            
        }
        if($quantity!=0){
            $quantity=$quantity-1;
        }
        $data=array(
            'quantity' => $quantity
        );
        $this->db->where('user_uid', $user_id);
        $this->db->where('coupon_type_uid', $coupon_type_id);
        $this->db->update('cou_coupon_onhand', $data);
		
        
        //check whether coupon_id has been used or not
        $this->db->select('coupon_number');
        $this->db->from('app_app_coupon');
        $this->db->where('coupon_number', $coupon_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            //log the coupon id has been used before, do not recommend to accept
            $content=" [Duplicate] Coupon No.".$coupon_id." has already been used.";
			$category="coupon_error";
            $this->log_model->update_log($content,$category);
            $success=false;
        }
		
		
          
		return $success;
    }
    
    function sequence_checking($coupon_type_uid, $scan_or_input_coupon_number) {
        //Find the stocked in coupons sequences of the coupon type
        $user_uid = 1; //Sam account's user uid
        
        //Selec all the coupon ohhand of SAM of particular coupon type
        $this->db->select('coupon_onhand.*, coupon_type.*');
        $this->db->from('cou_coupon_onhand coupon_onhand');
        $this->db->join('cou_coupon_type coupon_type', 'coupon_type.coupon_type_uid = coupon_onhand.coupon_type_uid' , 'left');
        $this->db->where('coupon_onhand.user_uid', $user_uid);
        $this->db->where('coupon_onhand.coupon_type_uid', $coupon_type_uid);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            //Check whether it matches any sequence
            $sam_onhand_coupon_result = $query->result();
            
            //For each result, check if the sequence is inside
            foreach($sam_onhand_coupon_result as $onhand_result){
                $sequence_start = $onhand_result->sequence_start;
                $sequence_end = $onhand_result->sequence_end;
                $start = $onhand_result->sequence_start_digit;
                $end = $onhand_result->sequence_end_digit;

                $length = (int) $end - (int) $start + 1;
                $sequence_substring = substr($scan_or_input_coupon_number, $start-1, $length);
                $sequence_start_substring = substr($sequence_start, $start-1, $length);
                $sequence_end_substring = substr($sequence_end, $start-1, $length);
            
                $success = false;
                //Check if it is a number
                if (!is_numeric($sequence_substring)){
//                    echo "format_error";
                    return false;
                } else if ((int)$sequence_substring >= (int)$sequence_start_substring && (int)$sequence_substring <= (int)$sequence_end_substring){
//                    echo "success";
                    $success = true;
                    return true;
                }
            }
            return $success;
        } else {
            return false;
        }
    }
}
