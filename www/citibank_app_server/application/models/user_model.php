<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class User_model extends CI_Model {

    var $table_log='log';
    
    var $table_name = 'usr_user';
    

    public function __construct() {
        parent::__construct();
        $this->load->library('userrole/UserModule');
    }

    function get_by_primary_key($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('user_uid' => $key));
        return $query->result();
    }
    
    //log in trigered in the first step serve for login in the controller
    public function login($username,$password) {
        $login_success=false;
        if($this->usermodule->login_session($username, md5($password))){
		$user_check_id = $this->usermodule->get_user_role();
		if($this->usermodule->is_salesman($user_check_id)){
            $login_success = $this->usermodule->login_procedure();
            $this->session->set_userdata('authorized', true);
        }
		}
        return $login_success;
    }

    //log out triggered if app is stopped
    public function logout(){
	
	$app_id=$this->session->userdata('application_id');
	$application_uid = 0;
	$application_uid_hello=0;
	$application_uid_dsds=0;
	$coupon_number=0;
    $query = $this->db->get_where('app_application', array('app_id' => $app_id));
    foreach ($query->result() as $row){
        $application_uid = $row->application_uid;
    }
	$query_app = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid));
	foreach ($query_app->result() as $row){
        $coupon_number = $row->coupon_number;
    }
	if ($coupon_number == NULL){
	$this->db->delete("app_application",array('app_id'=>$app_id));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	}
	$this->db->delete("app_app_coupon",array('coupon_number'=>"undefined"));
	$this->db->delete("app_app_coupon",array('coupon_number'=>NULL));
	$this->db->delete("app_application",array('app_id'=>NULL));
	if ( (strlen($coupon_number) == 0) || ($coupon_number == '0') || ($coupon_number == 'null')){
	$this->db->delete("app_app_coupon",array('coupon_number'=>$coupon_number));
	$this->db->delete("app_application",array('app_id'=>$app_id));
	}
	
	
	$query_e = $this->db->get_where('app_app_coupon', array('coupon_number' => NULL));
    foreach ($query_e->result() as $row){
        $application_uid_hello = $row->application_uid;
		$this->db->delete('app_application',array('application_uid'=>$application_uid_hello));
    }
	$this->db->select('application_uid');
    $this->db->from('app_application');
	$query_a = $this->db->get();
	 if ($query_a->num_rows() > 0){
        foreach($query_a->result() as $row){
			$application_uid_dsds = $row->application_uid;	
			$query_sasa = $this->db->get_where('app_app_coupon', array('application_uid' => $application_uid_dsds));
			
			if ($query_sasa->num_rows() == 0){
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			}
			if ($query_sasa->num_rows() > 0){
        foreach($query_sasa->result() as $row){
		    $cp = $row->coupon_number;
			if($cp == NULL){
			
			$this->db->delete("app_app_coupon",array('coupon_number'=>$cp));
			$this->db->delete("app_application",array('application_uid'=>$application_uid_dsds));
			}
		}
			}
        }
	 }
	
	
	
	
	
	
	
	
	
	
        $this->session->set_userdata('authorized', false);
		$this->session->set_userdata('application_id',-1);
        $success=false;
        if($this->usermodule->logout_procedure()){
            $success=true;
        }
        return $success;
    }
   
    //checked if the user has already log in
    public function checked_if_login(){
        $is_login = $this->session->userdata('authorized');
        return $is_login;
    }   
    
    function get_by_user_login_id($user_login_id) {
        $query = $this->db->get_where($this->table_name, array('user_login_id' => $user_login_id));
        return $query;
    }
    
    public function get_username()
    {
        return $this->session->userdata('user_role_uid');
    }
    
    //enter old pw, enter new pw twice, pass if pw same for both time
    public function checked_old_password($old_password){
        $success=false;
        $username=$this->session->userdata('user_role_uid'); 
        $this->db->select('user_password');
        $this->db->from('usr_user');
        $this->db->where('user_login_id', $username); 
        $this->db->where('user_password', md5($old_password)); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            $success=true;
        }
        return $success;
    }
    
    
    public function reset_new_password($new_password1,$new_password2){
        $success=false;
        $username=$this->session->userdata('user_role_uid');
        if ($new_password1==$new_password2){
            $data = array(
               'user_password' => md5($new_password1)  
            );
            $this->db->where('user_login_id', $username);
            $this->db->update('usr_user', $data); 
            $success=true;
        }
        return $success;
    }
    
}
