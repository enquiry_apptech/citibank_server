<?php

class AM_model extends CI_Model {

    /*
     * Columns and table name
     */
    var $am_uid = '';
    var $manager_uid = '';
    var $max_coupon_value = '';
    var $user_uid = '';
    var $am_team = '';
    
    var $table_name = 'rol_am';

    const role_table_name = 'usr_role';
    const userrole_table_name = 'usr_userrole';
    
    const allow_edit = true;
    const allow_delete = true;
    const uid_field_name = 'am_uid';
    const uid_column_name = 'am_uid';
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
    }
    
    function get_by_primary_key($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array($this::uid_field_name => $key));
        return $query->result();
    }
    
    function get_by_primary_key_with_user($uid){
        $this->db->select('am.*, user_table.*');
        $this->db->from('rol_am am');
        $this->db->join('usr_user user_table', 'am.user_uid = user_table.user_uid' , 'left');
        $this->db->where('am.am_uid', $uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_by_user_uid($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('user_uid' => $key));
        return $query;
    }
    
    function get_by_manager_uid($uid) {
        $key = (int)$uid;
        $query = $this->db->get_where($this->table_name, array('manager_uid' => $key));
        return $query;
    }
    
    function get_all(){
        $this->db->select('am.*');
        $this->db->from('rol_am am');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_for_coupon_distribution(){
        $this->db->select('am.*, user_table.*');
        $this->db->from('rol_am am');
        $this->db->join('usr_user user_table', 'am.user_uid = user_table.user_uid' , 'left');
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_for_coupon_distribution_by_manager_uid($manager_uid){
        $this->db->select('am.*, user_table.*');
        $this->db->from('rol_am am');
        $this->db->join('usr_user user_table', 'am.user_uid = user_table.user_uid' , 'left');
        $this->db->where('am.manager_uid', $manager_uid);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_listall_field_setting(){
        /*
         * Setup this array, then you can control the display of the column
         * control the type of column
         */
        $setting = array(
            $this::uid_field_name => array( "display" => true, "type" => "text"),
            "manager_uid" => array("display" => false, "type" => "text"),
            "max_coupon_value" => array("display" => false, "type" => "text"),
            "user_uid" => array("display" => false, "type" => "text")
        );
        
        return $setting;
    }
    
    function get_create_field_setting(){
        $this->db->select('manager.team option_name, manager.manager_uid option_value');
        $this->db->from('rol_manager manager');
        $this->db->join('usr_user user_table', 'manager.user_uid = user_table.user_uid' , 'left');
        
        $query = $this->db->get();
        $data_array = $query->result();
        
        //Roles in the project
        $option_array = array(
            array("option_name"=>"Manager", "option_value"=>"manager", "selected"=>false),
            array("option_name"=>"AM", "option_value"=>"am", "selected"=>true),
            array("option_name"=>"SE", "option_value"=>"se", "selected"=>false)
            );
        $object = json_decode(json_encode($option_array), FALSE);
        
        $setting = array(
            "role_uid" => array("display" => true, "type" => "dropdown", "required" => true, "list"=>$object),
            $this::uid_field_name => array("display" => false, "type" => "text", "required" => true),
            "manager_uid" => array( "display" => true, "type" => "dropdown", "required" => false, "list"=>$data_array),
            "am_team" => array("display" => true, "type" => "text", "required" => true),
            "max_coupon_value" => array("display" => true, "type" => "text", "required" => true, "placeholder"=>"HKD$")
        );
        
        return $setting;
    }
    
    function get_edit_field_setting(){
        $this->db->select('manager.team option_name, manager.manager_uid option_value');
        $this->db->from('rol_manager manager');
        $this->db->join('usr_user user_table', 'manager.user_uid = user_table.user_uid' , 'left');
        
        $query = $this->db->get();
        $data_array = $query->result();
        
        $setting = array(
            $this::uid_field_name => array("display" => false, "type" => "text", "required" => true),
            "manager_uid" => array( "display" => true, "type" => "dropdown", "required" => false, "list"=>$data_array),
            "am_team" => array("display" => true, "type" => "text", "required" => true),
            "max_coupon_value" => array("display" => true, "type" => "text", "required" => true, "placeholder"=>"HKD$"),
            "user_uid" => array( "display" => false, "type" => "text", "required" => false)
        );
        
        return $setting;
    }
    
    function get_field_translation($language){
        if ($language=="tc"){
            $role_uid = 'Title';
            $am_uid = 'ID';
            $manager_uid = 'Manager Team Name';
            $max_coupon_value = 'Max Coupon Value';
            $am_team = 'AM Team Name';
        } else {
            $role_uid = 'Title';
            $am_uid = 'ID';
            $manager_uid = 'Manager Team Name';
            $max_coupon_value = 'Max Coupon Value';
            $am_team = 'AM Team Name';
        }
        
        /*
         * Setup this array, then you can control the display of the column
         * control the type of column
         */
        $translation_array = array(
            "role_uid" => array("value" => $role_uid),
            $this::uid_field_name => array("value" => $am_uid),
            "manager_uid" => array("value" => $manager_uid),
            "max_coupon_value" => array("value" => $max_coupon_value),
            "am_team" => array("value" => $am_team)
        );

        return $translation_array;
    }
    
    /*
     * Basic Model Operation
     */
    function values(){
        $value_array = array(
            $this::uid_field_name => $this->am_uid,
            "manager_uid" => $this->manager_uid,
            "max_coupon_value" => $this->max_coupon_value,
            "user_uid" => $this->user_uid,
            "am_team" => $this->am_team
        );
        
        return $value_array;
    }
    
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    /*
     * Basic DB Operation
     */
    function insert($new_model) {
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this->table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $new_model->set_value($this::uid_field_name, $new_uid);
        $this->db->insert('rol_am', $new_model->values());
        
        return $new_uid;
    }
    
    function insert_user_role($user_uid, $role_uid){
        //Request a new uid
        $new_uid_result = $this->uidgenerator->request_uid($this::userrole_table_name);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        
        $value_array = array("usr_userrole_uid"=>$new_uid,"user_uid"=>$user_uid,"role_uid"=>$role_uid);
        $this->db->insert($this::userrole_table_name, $value_array);
        
        return $new_uid;
    }

    function delete($uid){
        $uid_delete=(int)$uid;
        $this->db->delete($this->table_name, array($this::uid_field_name => $uid_delete));
    }
    
    function delete_userrole($uid){
        $uid_delete=(int)$uid;
        $this->db->delete('usr_userrole', array('user_uid' => $uid_delete));
    }
    
     function update($model){
        $new_array = $model->values();
        
        $uid_field_name = $this::uid_field_name;
        $uid = $model->$uid_field_name;
            $this->db->where($this::uid_field_name, $uid);
           $this->db->update($this->table_name, $new_array);

    }
}
?>