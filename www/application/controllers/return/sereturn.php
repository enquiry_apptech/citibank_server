<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Sereturn extends CI_Controller {
    
    const model_name = 'sereturn_model';
    const user_model_name='user_model';
    const model_log='log_model';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model($this::model_name);
        $this->load->model($this::user_model_name);
        $this->load->model($this::model_log);
        $this->model_object = $this->sereturn_model;
        $this->load->library('userrole/UserModule');
    }
    
    //return handler, but only change cou_coupon_receive but not couu_coupon_onhand
    public function return_coupon(){
        $success = true;
		$coupon_id=$_POST['coupon_id'];
        $coupon_type_id=$_POST['coupon_type_id'];
        if($this->user_model->checked_if_login()){
        $user_role_id=$this->usermodule->get_user_role();
        $am_id = $this->model_object->find_related_am($user_role_id);
        $success=$this->model_object->return_coupon($user_role_id, $am_id, $coupon_type_id,$coupon_id);
		$this->model_object->sequence_checking($coupon_type_id, $coupon_id);
        }
        echo $success;
    }
    
    //get coupon type according to user_role_id stored when login 
    public function get_coupon_type(){
        $stack = array();
        if($this->user_model->checked_if_login()){
            $role_id = $this->usermodule->get_user_role();
            $stack=$this->model_object->find_coupon_type($role_id);
            echo json_encode($stack);
        }
        else{
            echo null;
        }
    }
}