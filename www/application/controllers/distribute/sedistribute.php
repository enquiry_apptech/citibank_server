<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Sedistribute extends CI_Controller {
    
    var $application_id;
    
    const model_name = 'sedistribute_model';
    const user_model_name='user_model';
    const model_log='log_model';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model($this::model_name);
        $this->load->model($this::user_model_name);
        $this->load->model($this::model_log);
        $this->model_object = $this->sedistribute_model;
        $this->load->library('userrole/UserModule');
    }
   
    //used in the list all, show the application id
    //should set application id first, then use
    public function Send_App_ID(){
        echo $this->session->userdata('application_id');
    }
    
    //called when scan application id
    //f:if_appid_used
    //f:set_value(application_id, $app_id)
    public function Store_AppID(){
        $success = false;
        if($this->user_model->checked_if_login()){
        //add $_post to get the mobile data
        $app_id=$_POST['app_id'];
        if($this->model_object->if_appid_used($app_id)){
            //log the application id has been used before, the app terminate immediately
            echo false;
        }
        else{
            $this->model_object->store_application_id($app_id);
            $success=true;
        }
        }
        echo $success;   
    }   
    
    //search Coupon Type by Application Type
    //f:search_coupon_info(($app_type_id)
    public function Search_Coupon_Type($app_type_id){
        //$app_type_id=$_POST['app_type_id'];
        
        //if($this->user_model->checked_if_login()){
        //add $_post to get the mobile data
        $stack_array=$this->model_object->search_coupon_info($app_type_id);
        //return $stack_array;
        echo json_encode($stack_array);
        //}
        //else{
        //    echo null;
            //redirect to login page
        //}
    }
    
    //triggered after scanning the coupon
    //return true or false if coupon used successfully or not
    //f:if_coupid_used($coupon_id)
    //f:store_used_coupon($application_id, $coupon_id)
    public function Store_Used_Coupon(){
        $coupon_id=$_POST['coupon_id'];
        $coupon_type_id=$_POST['coupon_type_id'];
		$app_type_id=$_POST['app_type_id'];
        $success=false;
        if($this->user_model->checked_if_login()){
        
        if($this->model_object->if_coupid_used($coupon_id)){
            //log the coupon has been used then stop
            //return false;
        }
        else{
            $application_id=$this->session->userdata('application_id');
            if($this->model_object->store_used_coupon($application_id, $app_type_id, $coupon_id,$coupon_type_id)){
                $success=true;
            }
            else{
                
            }
        }
        }
        echo $success;
    }
    
    //search all the coupon simply by application id, used in show
    //triggered after finish
    //f:list_all_coupon($app_id) 
    public function Show_All_Coupon(){
        $stack = array();
        if($this->user_model->checked_if_login()){
        $application_id=$this->session->userdata('application_id');
        $stack=$this->model_object->list_all_coupon($application_id);
        $this->user_model->logout();
		$this->session->set_userdata('application_id',-1);
		
        //return $stack;
        echo json_encode($stack);
        }
        else{
            //redirect to login page
            echo null;
        }
    }
}

