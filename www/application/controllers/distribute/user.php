<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class User extends CI_Controller{
    const model_name = 'user_model';
    const model_log='log_model';
    const model_dis='sedistribute_model';
    
    public function __construct(){
        parent::__construct();
        $this->load->model($this::model_name);
        $this->load->model($this::model_log);
        $this->load->model($this::model_dis);
        $this->model_object = $this->user_model;
    }
    
   
    //handle the login page
    public function login(){
        //username and password is posted from mobile app
        //return false, ask the user to change password, otherwise the user cannot log in
        $username=$_POST['username'];
        $password=$_POST['password'];
        $this->session->set_userdata('user_role_uid', $username);
        $success=false;
        if($this->sedistribute_model->if_password_not_changed($username, $password)){
        if($this->model_object->login($username,$password)){
            
             $success=true;
        }
        }
        echo $success;
    }
    
    public function reset_password(){
        $old_password=$_POST['old_password'];
        $new_password1=$_POST['new_password1'];
        $new_password2=$_POST['new_password2'];
        $success=false;
        if($this->model_object->checked_old_password($old_password)){
            $this->model_object->reset_new_password($new_password1,$new_password2);
            $success=true;
        }
        echo $success;
    }
    
}

