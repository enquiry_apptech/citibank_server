<?php
class Log_model extends CI_Model {
    
    const user_model_name='user_model';
    
    var $table_log='log';
    var $table_application = 'app_application';
    var $table_coupon_used = 'coupon_used';
    var $table_coupon_type = 'cou_coupon_type';
    var $table_application_type='application_type';
    var $log_content = ' ';
    
    var $application_id=null;
    
    
    function __construct() {
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
        $this->load->model($this::user_model_name);
    }
    
    //to check whether this application id has been used
    public function update_log($log_content,$category) {
	    $time= date('Y-m-d H:i:s');
		$user_uid=1;
        $username=$this->user_model->get_username();
        $new_uid_result = $this->uidgenerator->request_uid($this->table_log);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
         $data=array(
            'uid' => $new_uid,
            'logs' => $username.' : '.$log_content,
            'time' => $time,
			'category' => $category,
			'user_uid' => $user_uid
                );  
         $this->db->insert('log',$data);
         return true;  
    }
    
}