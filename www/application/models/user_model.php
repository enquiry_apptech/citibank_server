<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class User_model extends CI_Model {

    //should change auth=false when done
    var $table_log='log';

    public function __construct() {
        parent::__construct();
        $this->load->library('userrole/UserModule');
    }

    //log in trigered in the first step serve for login in the controller
    public function login($username,$password) {
        $login_success=false;
        if($this->usermodule->login_session($username, md5($password))){
            $login_success = $this->usermodule->login_procedure();
            $this->session->set_userdata('authorized', true);
        }
        return $login_success;
    }

    //log out triggered if app is stopped
    public function logout(){
        $this->session->set_userdata('authorized', false);
		$this->session->set_userdata('application_id',-1);
        $success=false;
        if($this->usermodule->logout_procedure()){
            return $success;
        }
        return $success;
    }
   
    //checked if the user has already log in
    public function checked_if_login(){
        $is_login = $this->session->userdata('authorized');
        return $is_login;
    }   
    
    public function get_username()
    {
        return $this->session->userdata('user_role_uid');
    }
    
    //enter old pw, enter new pw twice, pass if pw same for both time
    public function checked_old_password($old_password){
        $success=false;
        $username=$this->session->userdata('user_role_uid'); 
        $this->db->select('user_password');
        $this->db->from('usr_user');
        $this->db->where('user_login_id', $username); 
        $this->db->where('user_password', md5($old_password)); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            $success=true;
        }
        return $success;
    }
    
    
    public function reset_new_password($new_password1,$new_password2){
        $success=false;
        $username=$this->session->userdata('user_role_uid');
        if ($new_password1==$new_password2){
            $data = array(
               'user_password' => md5($new_password1)  
            );
            $this->db->where('user_login_id', $username);
            $this->db->update('usr_user', $data); 
            $success=true;
        }
        return $success;
    }
    
}
