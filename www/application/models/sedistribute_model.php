<?php
class Sedistribute_model extends CI_Model {
    
    var $table_application = 'app_application';
    var $table_coupon_used = 'app_app_coupon';
    var $table_coupon_type = 'cou_coupon_type';
    var $table_application_type='application_type';
    var $table_coupon_onhand = 'cou_coupon_onhand';
       
    const model_log='log_model';
    
    
    function __construct() {
        parent::__construct();
        $this->load->library('uidgenerator/UidGenerator');
        $this->load->library('userrole/UserModule');
        $this->load->model($this::model_log);
    }
    
    
    //check whether the username is equal to the password 
    function if_password_not_changed($username, $password){
        $success=true;
        if(md5($username)==md5($password)){
            $success=false;
        }
        return $success;
    }
    
    //to check whether this application id has been used
    function if_appid_used($application_id) {
        $used=false;
		$this->db->select('app_id');
        $this->db->from('app_application');
        $this->db->where('app_id', $application_id);
        $query_s = $this->db->get();
        if ($query_s->num_rows() > 0) {
            $used=true;
			$category="Coupon_ERROR";
            $content="application id:".$application_id."has already been used, however been used another time.";
            $this->log_model->update_log($content,$category);
        }
		
        return $used;   
    }
    
    //store application id and the related salesman seoid
    function store_application_id($application_id){
        
        $this->session->set_userdata('application_id',$application_id);
        $user_uid=$this->usermodule->get_user_role();
        $new_uid_result = $this->uidgenerator->request_uid($this->table_application);
        $new_uid = $new_uid_result[0]->uid_gen_current_uid;
        $date=date("l");
        $data = array(
            'application_uid' => $new_uid ,
            'user_uid' => $user_uid,
            'app_id' => $application_id,
            'app_create_date' => $date);
        $this->db->insert('app_application', $data);
    }
    
    //to check whether this coupon_id bar scanned result has been used
    function if_coupid_used($coupon_id){
        $used=false;
        $query = $this->db->get_where('app_app_coupon', array('coupon_number' => $coupon_id));
        if ($query->num_rows() > 0){
            $used=true;
            $content="The Coupon ID No.".$coupon_id."has already been used, however been used another time.";
			$category="Coupon ERROR";
            $this->log_model->update_log($content,$category);
        }
        return $used;
    }
    
    //this place should write a function to check whether it is in the coupon list
    //coupon is generated using a series of numbers
    //undone
    
    //used for set application_id
    function set_value($fieldname, $value) {
        $this->$fieldname = $value;
    }
    
    //search coupon's info based on its quantity ? > 0 for checking on At->Ct
    function search_coupon_info($app_type_id){
        $user_uid=1014;
	    $end_date;
	    $start_date;
        //search coupon_type_id according to app_type_id in cou_offer_update table
        $coupon_type_uid;
        $stack = array();
        $quantity = -1;
        $user_uid=$this->usermodule->get_user_role();
        $query = $this->db->get_where('cou_offer_update', array('application_type_uid' => $app_type_id));
        foreach ($query->result() as $row){
            $coupon_type_uid = $row->coupon_type_uid;
	    $this->db->select('start_date, end_date');
            $this->db->from('cou_offer_update');
            $this->db->where('coupon_type_uid', $coupon_type_uid);
            $this->db->where('application_type_uid', $app_type_id);
            $query_date = $this->db->get();
             if ($query_date->num_rows() > 0){
            foreach ($query_date->result() as $row){
                $start_date = $row->start_date;
                $end_date = $row->end_date;
            }
            }
            
            //get quantity according to the coupon_type_uid
            $this->db->select('quantity');
            $this->db->from('cou_coupon_onhand');
	    $this->db->where('coupon_type_uid', $coupon_type_uid);
	    $this->db->where('user_uid', $user_uid);
            $query_quantity = $this->db->get();
            if ($query_quantity->num_rows() > 0){
            foreach ($query_quantity->result() as $row){
                $quantity = $row->quantity;
            }
            }
			else{
			$quantity=0;
			}
            if($quantity > 0){
			
            //search coupon information based on the coupon_type_uid   
            $this->db->select('value,name,scanning_instruction,scanable');
            $this->db->from('cou_coupon_type');
            $this->db->where('coupon_type_uid', $coupon_type_uid);    			
            $query_coupon_type = $this->db->get();
           
            if ($query_coupon_type->num_rows() > 0){
			    date_default_timezone_set('Asia/Hong_Kong');
				$format = 'Y-m-d H:i:s';
                $date = DateTime::createFromFormat($format, $end_date);
				$interval = date_diff(new DateTime(), $date);
				if($interval->invert == 0){
                foreach ($query_coupon_type->result() as $row){
                        $data = array(
                            "coupon_type_uid" => $coupon_type_uid,
                            "value" =>$row->value ,
                            "name" => $row->name,
                            'scanning_instruction' => $row->scanning_instruction,
                            "scanable" => $row->scanable);
                        array_push($stack, $data);   
                    }
                  }
				}
                    
            }
			else{
                        
                    }
        }    
        return $stack;
    }
    
    //used in determine whether transaction is successfully or not?
    function store_used_coupon($application_id, $app_type_id, $coupon_id,$coupon_type_id){
        $success  = true;
        $quantity = 0;
        $user_uid = $this->usermodule->get_user_role();
        //search application uid
        $application_uid = 0;
        $query = $this->db->get_where('app_application', array('app_id' => $application_id));
        foreach ($query->result() as $row){
            $application_uid = $row->application_uid;
        }
         
        //inserting coupon used information in app_app_coupon table
        $new_uid_result_coupon_used = $this->uidgenerator->request_uid($this->table_coupon_used);
        $new_uid_coupon_used = $new_uid_result_coupon_used[0]->uid_gen_current_uid;
        $coupon_used_data=array(
            'app_coupon_uid'  => $new_uid_coupon_used ,
			'app_type_uid' => $app_type_id,
            'application_uid' => $application_uid ,
            'coupon_type_uid' => $coupon_type_id,
            'coupon_number'  => $coupon_id);
        $this->db->insert('app_app_coupon', $coupon_used_data);
        
        
        //change cou_coupon_type quantity
        $this->db->select('quantity');
        $this->db->from('cou_coupon_onhand');
        $this->db->where('user_uid', $user_uid); 
        $this->db->where('coupon_type_uid', $coupon_type_id); 
        $query_quantity = $this->db->get();
        if ($query_quantity->num_rows() > 0){
            foreach ($query_quantity->result() as $row){
                $quantity=$row->quantity;
            }
        }
        else{
            //log used unauthorized coupon_type_id
             $content="coupon_type_id No.".$coupon_type_id."is not authorized, please check with this Salesman.";
			 $category="Coupon_ERROR";
             $this->log_model->update_log($content,$category);
        }
        if($quantity!=0){
            $quantity=$quantity-1;
        }
        $data=array(
            'quantity' => $quantity
        );
        $this->db->where('user_uid', $user_uid);
        $this->db->where('coupon_type_uid', $coupon_type_id);
        $this->db->update('cou_coupon_onhand', $data);
        
        return $success;
    }
    
    //search the database about all the coupon information based on 
    //the application id, used in the listall, final step
    function list_all_coupon($app_id){
        $this->db->select('cou_coupon_type.coupon_type_uid,value,name');
        $this->db->from('cou_coupon_type');
        $this->db->join('app_app_coupon', 'app_app_coupon.coupon_type_uid = cou_coupon_type.coupon_type_uid' , 'left');
        $this->db->join('app_application', 'app_application.application_uid = app_app_coupon.application_uid' , 'left');
        $this->db->where('app_application.app_id', $app_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
        else{
            $content="The Coupon Cart in application ID No.".$app_id."is empty, please check with this Salesman.";
	    $category="Coupon_ERROR";
            $this->log_model->update_log($content,$category);
            return null; 
        }
    }
    
}
