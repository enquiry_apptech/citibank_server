CREATE DATABASE  IF NOT EXISTS `citibank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `citibank`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: citibank
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_app_coupon`
--

DROP TABLE IF EXISTS `app_app_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_app_coupon` (
  `app_coupon_uid` int(11) NOT NULL,
  `app_uid` int(11) DEFAULT NULL,
  `coupon_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`app_coupon_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_app_coupon`
--

LOCK TABLES `app_app_coupon` WRITE;
/*!40000 ALTER TABLE `app_app_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_app_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_application`
--

DROP TABLE IF EXISTS `app_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_application` (
  `application_uid` int(11) NOT NULL,
  `app_id` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`application_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_application`
--

LOCK TABLES `app_application` WRITE;
/*!40000 ALTER TABLE `app_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_coupon_onhand`
--

DROP TABLE IF EXISTS `cou_coupon_onhand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_coupon_onhand` (
  `coupon_onhand_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_onhand_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_coupon_onhand`
--

LOCK TABLES `cou_coupon_onhand` WRITE;
/*!40000 ALTER TABLE `cou_coupon_onhand` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_coupon_onhand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_coupon_type`
--

DROP TABLE IF EXISTS `cou_coupon_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_coupon_type` (
  `coupon_type_uid` int(11) NOT NULL,
  `value` int(8) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `name` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`coupon_type_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_coupon_type`
--

LOCK TABLES `cou_coupon_type` WRITE;
/*!40000 ALTER TABLE `cou_coupon_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_coupon_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_distribute`
--

DROP TABLE IF EXISTS `cou_distribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_distribute` (
  `distribute_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `receiver_user_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `accepted` int(1) DEFAULT NULL,
  PRIMARY KEY (`distribute_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_distribute`
--

LOCK TABLES `cou_distribute` WRITE;
/*!40000 ALTER TABLE `cou_distribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_distribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_return`
--

DROP TABLE IF EXISTS `cou_return`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_return` (
  `return_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `receiver_user_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `accepted` int(1) DEFAULT NULL,
  PRIMARY KEY (`return_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_return`
--

LOCK TABLES `cou_return` WRITE;
/*!40000 ALTER TABLE `cou_return` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_return` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_image`
--

DROP TABLE IF EXISTS `sys_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_image` (
  `image_uid` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(1024) DEFAULT NULL,
  `image_desc` varchar(1024) DEFAULT NULL,
  `image_location` varchar(1024) DEFAULT NULL,
  `imagetype_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1281 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_image`
--

LOCK TABLES `sys_image` WRITE;
/*!40000 ALTER TABLE `sys_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_param`
--

DROP TABLE IF EXISTS `sys_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_param` (
  `param_uid` int(11) NOT NULL,
  `upload_img_path` varchar(1024) NOT NULL,
  PRIMARY KEY (`param_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_param`
--

LOCK TABLES `sys_param` WRITE;
/*!40000 ALTER TABLE `sys_param` DISABLE KEYS */;
INSERT INTO `sys_param` VALUES (1,'/citibank_uploaded_image/img/');
/*!40000 ALTER TABLE `sys_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_uid_gen`
--

DROP TABLE IF EXISTS `sys_uid_gen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_uid_gen` (
  `uid_gen_table_name` varchar(128) NOT NULL,
  `uid_gen_current_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid_gen_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_uid_gen`
--

LOCK TABLES `sys_uid_gen` WRITE;
/*!40000 ALTER TABLE `sys_uid_gen` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_uid_gen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_role`
--

DROP TABLE IF EXISTS `usr_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_role` (
  `role_uid` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  `role_desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_role`
--

LOCK TABLES `usr_role` WRITE;
/*!40000 ALTER TABLE `usr_role` DISABLE KEYS */;
INSERT INTO `usr_role` VALUES (1,'SAM',NULL),(2,'Manager',NULL),(3,'AM',NULL),(4,'SE',NULL);
/*!40000 ALTER TABLE `usr_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_user`
--

DROP TABLE IF EXISTS `usr_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_user` (
  `user_uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(1024) DEFAULT NULL,
  `user_last_name` varchar(1024) DEFAULT NULL,
  `user_nick_name` varchar(1024) DEFAULT NULL,
  `user_email` varchar(1024) DEFAULT NULL,
  `user_tel` varchar(45) DEFAULT NULL,
  `user_gender` varchar(45) DEFAULT NULL,
  `user_login_id` varchar(1024) DEFAULT NULL,
  `user_password` varchar(1024) DEFAULT NULL,
  `user_default_password` varchar(1024) DEFAULT NULL,
  `user_verified` int(1) DEFAULT '0',
  `user_last_login` timestamp NULL DEFAULT NULL,
  `last_update_user` varchar(1024) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `create_user` varchar(45) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1042 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_user`
--

LOCK TABLES `usr_user` WRITE;
/*!40000 ALTER TABLE `usr_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `usr_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_userrole`
--

DROP TABLE IF EXISTS `usr_userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_userrole` (
  `usr_userrole_uid` int(11) NOT NULL,
  `user_uid` int(11) NOT NULL,
  `role_uid` int(11) NOT NULL,
  PRIMARY KEY (`usr_userrole_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_userrole`
--

LOCK TABLES `usr_userrole` WRITE;
/*!40000 ALTER TABLE `usr_userrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `usr_userrole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-15 16:09:59
