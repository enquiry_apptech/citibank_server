CREATE DATABASE  IF NOT EXISTS `citibank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `citibank`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: citibank
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_app_coupon`
--

DROP TABLE IF EXISTS `app_app_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_app_coupon` (
  `app_coupon_uid` int(11) NOT NULL,
  `app_uid` int(11) DEFAULT NULL,
  `coupon_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`app_coupon_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_app_coupon`
--

LOCK TABLES `app_app_coupon` WRITE;
/*!40000 ALTER TABLE `app_app_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_app_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_application`
--

DROP TABLE IF EXISTS `app_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_application` (
  `application_uid` int(11) NOT NULL,
  `app_id` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`application_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_application`
--

LOCK TABLES `app_application` WRITE;
/*!40000 ALTER TABLE `app_application` DISABLE KEYS */;
INSERT INTO `app_application` VALUES (1,'1'),(11,'11'),(1001,'2'),(1002,'2'),(1003,'2'),(1004,'2'),(1005,'2'),(1006,'2'),(1007,'2'),(1008,'2'),(1009,'2'),(1010,'2'),(1011,'2');
/*!40000 ALTER TABLE `app_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application_type`
--

DROP TABLE IF EXISTS `application_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_type` (
  `app_type_uid` int(11) NOT NULL,
  `app_type_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`app_type_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_type`
--

LOCK TABLES `application_type` WRITE;
/*!40000 ALTER TABLE `application_type` DISABLE KEYS */;
INSERT INTO `application_type` VALUES (1000,'cheque'),(1001,'cash');
/*!40000 ALTER TABLE `application_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_coupon_onhand`
--

DROP TABLE IF EXISTS `cou_coupon_onhand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_coupon_onhand` (
  `coupon_onhand_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `sequence_start` varchar(1024) DEFAULT NULL,
  `sequence_end` varchar(1024) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`coupon_onhand_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_coupon_onhand`
--

LOCK TABLES `cou_coupon_onhand` WRITE;
/*!40000 ALTER TABLE `cou_coupon_onhand` DISABLE KEYS */;
INSERT INTO `cou_coupon_onhand` VALUES (1001,1,1002,1,'aa300','aa400',NULL),(1002,1,1002,8,'aa1','aa9','2014-04-09 00:00:00'),(1003,1,1002,11,'','','2014-04-01 00:00:00'),(1004,1,1002,11,'','','2014-04-04 00:00:00'),(1005,1,1002,11,'','','2014-04-10 00:00:00'),(1006,1,1002,100,'aa300','aa399','2014-04-23 00:00:00');
/*!40000 ALTER TABLE `cou_coupon_onhand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_coupon_receive`
--

DROP TABLE IF EXISTS `cou_coupon_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_coupon_receive` (
  `coupon_receive_uid` int(11) NOT NULL,
  `distributer_uid` int(11) DEFAULT NULL,
  `receiver_uid` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `distribute_date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`coupon_receive_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_coupon_receive`
--

LOCK TABLES `cou_coupon_receive` WRITE;
/*!40000 ALTER TABLE `cou_coupon_receive` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_coupon_receive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_coupon_type`
--

DROP TABLE IF EXISTS `cou_coupon_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_coupon_type` (
  `coupon_type_uid` int(11) NOT NULL,
  `value` int(8) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `name` varchar(1024) DEFAULT NULL,
  `app_type_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `scanning_instruction` varchar(2048) DEFAULT NULL,
  `sequence_start_digit` int(11) DEFAULT NULL,
  `sequence_end_digit` int(11) DEFAULT NULL,
  `scanable` int(1) DEFAULT NULL,
  PRIMARY KEY (`coupon_type_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_coupon_type`
--

LOCK TABLES `cou_coupon_type` WRITE;
/*!40000 ALTER TABLE `cou_coupon_type` DISABLE KEYS */;
INSERT INTO `cou_coupon_type` VALUES (0,NULL,NULL,NULL,NULL,-1,NULL,NULL,NULL,NULL),(1002,100,'0000-00-00 00:00:00','welcome',1000,2,'bottom right',3,5,1),(1003,50,'0000-00-00 00:00:00','Parkner Shop $50 coupon',1001,4,'bottom right scan',3,5,1);
/*!40000 ALTER TABLE `cou_coupon_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_distribute`
--

DROP TABLE IF EXISTS `cou_distribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_distribute` (
  `distribute_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `receiver_user_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `accepted` int(1) DEFAULT NULL,
  PRIMARY KEY (`distribute_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_distribute`
--

LOCK TABLES `cou_distribute` WRITE;
/*!40000 ALTER TABLE `cou_distribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_distribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cou_return`
--

DROP TABLE IF EXISTS `cou_return`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cou_return` (
  `return_uid` int(11) NOT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `receiver_user_uid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  `accepted` int(1) DEFAULT NULL,
  PRIMARY KEY (`return_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cou_return`
--

LOCK TABLES `cou_return` WRITE;
/*!40000 ALTER TABLE `cou_return` DISABLE KEYS */;
/*!40000 ALTER TABLE `cou_return` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_used`
--

DROP TABLE IF EXISTS `coupon_used`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_used` (
  `coupon_used_uid` int(11) NOT NULL,
  `coupon_used_id` int(11) DEFAULT NULL,
  `app_uid` int(11) DEFAULT NULL,
  `coupon_type_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_used_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_used`
--

LOCK TABLES `coupon_used` WRITE;
/*!40000 ALTER TABLE `coupon_used` DISABLE KEYS */;
INSERT INTO `coupon_used` VALUES (1000,12,12,12),(1003,13,1003,13),(1004,14,1004,14),(1005,15,1005,15),(1006,16,1006,16),(1007,17,1007,17),(1008,20,1008,20),(1009,21,1009,1003),(1010,45,1010,1002),(1011,46,1011,1002);
/*!40000 ALTER TABLE `coupon_used` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `uid` int(11) NOT NULL,
  `logs` text,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1001,' : the application id No.1has already been used, however been used another time.','838:59:59'),(1002,' : the application id No.1has already been used, however been used another time.','838:59:59'),(1003,' : the coupon id No.12has already been used, however been used another time.','838:59:59'),(1004,' : the coupon id No.12has already been used, however been used another time.','838:59:59'),(1005,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1006,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1007,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1008,' : the coupon id No.14has already been used, however been used another time.','838:59:59'),(1009,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1010,' : the coupon id No.15has already been used, however been used another time.','838:59:59'),(1011,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1012,' : the coupon id No.13has already been used, however been used another time.','838:59:59'),(1013,' : The User has been successfully Log Out.','838:59:59'),(1014,' : The User has been successfully Log Out.','838:59:59'),(1015,'01 : logged in','838:59:59'),(1016,'01 : logged in','838:59:59');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_am`
--

DROP TABLE IF EXISTS `rol_am`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_am` (
  `am_uid` int(11) NOT NULL,
  `max_coupon_value` varchar(1024) DEFAULT NULL,
  `manager_uid` int(11) DEFAULT NULL,
  `user_uid` int(11) DEFAULT NULL,
  `am_team` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`am_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_am`
--

LOCK TABLES `rol_am` WRITE;
/*!40000 ALTER TABLE `rol_am` DISABLE KEYS */;
INSERT INTO `rol_am` VALUES (1001,'300000',1004,1005,'AM Star'),(1002,'300000',1004,1006,'CC');
/*!40000 ALTER TABLE `rol_am` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_manager`
--

DROP TABLE IF EXISTS `rol_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_manager` (
  `manager_uid` int(11) NOT NULL,
  `team` varchar(45) DEFAULT NULL,
  `max_coupon_value` varchar(45) DEFAULT NULL,
  `user_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`manager_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_manager`
--

LOCK TABLES `rol_manager` WRITE;
/*!40000 ALTER TABLE `rol_manager` DISABLE KEYS */;
INSERT INTO `rol_manager` VALUES (1004,'CW','400000',1004);
/*!40000 ALTER TABLE `rol_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_se`
--

DROP TABLE IF EXISTS `rol_se`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_se` (
  `se_uid` int(11) NOT NULL,
  `max_coupon_value` varchar(1024) DEFAULT NULL,
  `agent_code` varchar(1024) DEFAULT NULL,
  `am_uid` int(11) DEFAULT NULL,
  `user_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`se_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_se`
--

LOCK TABLES `rol_se` WRITE;
/*!40000 ALTER TABLE `rol_se` DISABLE KEYS */;
INSERT INTO `rol_se` VALUES (1004,'3000','1234',1002,1008),(1005,'10000','239481241902184893004',1001,1009);
/*!40000 ALTER TABLE `rol_se` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_image`
--

DROP TABLE IF EXISTS `sys_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_image` (
  `image_uid` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(1024) DEFAULT NULL,
  `image_desc` varchar(1024) DEFAULT NULL,
  `image_location` varchar(1024) DEFAULT NULL,
  `imagetype_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_image`
--

LOCK TABLES `sys_image` WRITE;
/*!40000 ALTER TABLE `sys_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_param`
--

DROP TABLE IF EXISTS `sys_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_param` (
  `param_uid` int(11) NOT NULL,
  `upload_img_path` varchar(1024) NOT NULL,
  PRIMARY KEY (`param_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_param`
--

LOCK TABLES `sys_param` WRITE;
/*!40000 ALTER TABLE `sys_param` DISABLE KEYS */;
INSERT INTO `sys_param` VALUES (1,'/citibank_uploaded_image/img/');
/*!40000 ALTER TABLE `sys_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_uid_gen`
--

DROP TABLE IF EXISTS `sys_uid_gen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_uid_gen` (
  `uid_gen_table_name` varchar(128) NOT NULL,
  `uid_gen_current_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid_gen_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_uid_gen`
--

LOCK TABLES `sys_uid_gen` WRITE;
/*!40000 ALTER TABLE `sys_uid_gen` DISABLE KEYS */;
INSERT INTO `sys_uid_gen` VALUES ('application_type',1000),('app_application',1011),('app_app_coupon',1000),('coupon_used',1011),('cou_coupon_onhand',1006),('cou_coupon_receive',1000),('cou_coupon_type',1004),('cou_distribute',1000),('cou_return',1000),('log',1016),('rol_am',1008),('rol_manager',1004),('rol_se',1000),('usr_user',1012);
/*!40000 ALTER TABLE `sys_uid_gen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_role`
--

DROP TABLE IF EXISTS `usr_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_role` (
  `role_uid` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  `role_desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_role`
--

LOCK TABLES `usr_role` WRITE;
/*!40000 ALTER TABLE `usr_role` DISABLE KEYS */;
INSERT INTO `usr_role` VALUES (1,'SAM',NULL),(2,'Manager',NULL),(3,'AM',NULL),(4,'SE',NULL);
/*!40000 ALTER TABLE `usr_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_user`
--

DROP TABLE IF EXISTS `usr_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_user` (
  `user_uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(1024) DEFAULT NULL,
  `user_last_name` varchar(1024) DEFAULT NULL,
  `user_nick_name` varchar(1024) DEFAULT NULL,
  `user_email` varchar(1024) DEFAULT NULL,
  `user_tel` varchar(45) DEFAULT NULL,
  `user_gender` varchar(45) DEFAULT NULL,
  `user_login_id` varchar(1024) DEFAULT NULL,
  `user_password` varchar(1024) DEFAULT NULL,
  `user_default_password` varchar(1024) DEFAULT NULL,
  `user_verified` int(1) DEFAULT '0',
  `user_last_login` timestamp NULL DEFAULT NULL,
  `last_update_user` varchar(1024) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `create_user` varchar(45) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_user`
--

LOCK TABLES `usr_user` WRITE;
/*!40000 ALTER TABLE `usr_user` DISABLE KEYS */;
INSERT INTO `usr_user` VALUES (1,NULL,NULL,'SAM',NULL,NULL,NULL,'SAM','25d55ad283aa400af464c76d713c07ad',NULL,0,NULL,NULL,NULL,NULL,NULL),(1004,'','','Billy Chung','','','','412345a','d2805928abdc48cf6fc32766218a281e',NULL,0,'0000-00-00 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(1005,'','','Ben Chan','','','','123','202cb962ac59075b964b07152d234b70',NULL,0,'0000-00-00 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(1006,'','','Eric Cheung','','','','12343','',NULL,0,'0000-00-00 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(1008,'','','Billy Chung','','','','1234','',NULL,0,'0000-00-00 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(1009,'','','Amy','','','','12342134902','',NULL,0,'0000-00-00 00:00:00','','0000-00-00 00:00:00','','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `usr_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_userrole`
--

DROP TABLE IF EXISTS `usr_userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_userrole` (
  `usr_userrole_uid` int(11) NOT NULL,
  `user_uid` int(11) NOT NULL,
  `role_uid` int(11) NOT NULL,
  PRIMARY KEY (`usr_userrole_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_userrole`
--

LOCK TABLES `usr_userrole` WRITE;
/*!40000 ALTER TABLE `usr_userrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `usr_userrole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-02 19:29:02
